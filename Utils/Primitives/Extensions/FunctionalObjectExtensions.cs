namespace Primitives.Extensions;

public static class FunctionalObjectExtensions
{
    public static TResult As<TObject, TResult>(this TObject obj, Func<TObject, TResult> func) => 
        func(obj);
}