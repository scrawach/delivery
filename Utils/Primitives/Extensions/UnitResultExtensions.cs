using CSharpFunctionalExtensions;

namespace Primitives.Extensions;

public static class UnitResultExtensions
{
    public static UnitResult<Error> SuccessIf(this UnitResult<Error> result, bool condition, Error error)
    {
        if (result.IsFailure)
            return result;
        
        return condition 
            ? result 
            : UnitResult.Failure(error);
    }
}