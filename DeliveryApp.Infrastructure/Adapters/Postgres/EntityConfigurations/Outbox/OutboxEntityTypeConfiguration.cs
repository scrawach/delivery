using DeliveryApp.Infrastructure.Adapters.Postgres.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DeliveryApp.Infrastructure.Adapters.Postgres.EntityConfigurations.Outbox;

public class OutboxEntityTypeConfiguration : IEntityTypeConfiguration<OutboxMessage>
{
    public void Configure(EntityTypeBuilder<OutboxMessage> configuration)
    {
        configuration
            .ToTable("outbox");

        configuration
            .Property(entity => entity.Id)
            .ValueGeneratedNever()
            .HasColumnName("id");

        configuration
            .Property(entity => entity.Type)
            .UsePropertyAccessMode(PropertyAccessMode.Field)
            .HasColumnName("type")
            .IsRequired();

        configuration
            .Property(entity => entity.Content)
            .UsePropertyAccessMode(PropertyAccessMode.Field)
            .HasColumnName("content")
            .IsRequired();

        configuration
            .Property(entity => entity.OccuredOnUtc)
            .UsePropertyAccessMode(PropertyAccessMode.Field)
            .HasColumnName("occured_on_utc")
            .IsRequired();

        configuration
            .Property(entity => entity.ProcessedOnUtc)
            .UsePropertyAccessMode(PropertyAccessMode.Field)
            .HasColumnName("processed_on_utc")
            .IsRequired(false);
    }
}