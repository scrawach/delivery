using DeliveryApp.Infrastructure.Adapters.Postgres.Entities;
using MediatR;

namespace DeliveryApp.Infrastructure.Adapters.Postgres.BackgroundJobs;

public class ProcessInboxMessagesJob : ProcessMessagesJob<InboxMessage>
{
    public ProcessInboxMessagesJob(ApplicationDbContext dbContext, IMediator mediator) 
        : base(dbContext, mediator) { }
}