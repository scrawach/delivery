using DeliveryApp.Infrastructure.Adapters.Postgres.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Primitives;
using Quartz;

namespace DeliveryApp.Infrastructure.Adapters.Postgres.BackgroundJobs;

public class ProcessMessagesJob<TMessage> : IJob where TMessage : BaseMessage
{
    private const int AmountOfMessages = 20;
    
    private readonly ApplicationDbContext _dbContext;
    private readonly IMediator _mediator;

    public ProcessMessagesJob(ApplicationDbContext dbContext, IMediator mediator)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    public async Task Execute(IJobExecutionContext context)
    {
        var messages = await _dbContext
            .Set<TMessage>()
            .Where(message => message.ProcessedOnUtc == null)
            .OrderByDescending(message => message.OccuredOnUtc)
            .Take(AmountOfMessages)
            .ToArrayAsync(context.CancellationToken);

        foreach (var message in messages)
        {
            var domainEvent = JsonConvert.DeserializeObject<DomainEvent>(message.Content, 
                new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });

            await _mediator.Publish(domainEvent, context.CancellationToken);
            
            message.ProcessedOnUtc = DateTime.UtcNow;
        }

        if (messages.Length != 0)
            await _dbContext.SaveChangesAsync();
    }
}