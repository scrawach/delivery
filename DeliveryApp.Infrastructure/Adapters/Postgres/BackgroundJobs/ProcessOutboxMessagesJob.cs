using DeliveryApp.Infrastructure.Adapters.Postgres.Entities;
using MediatR;

namespace DeliveryApp.Infrastructure.Adapters.Postgres.BackgroundJobs;

public class ProcessOutboxMessagesJob : ProcessMessagesJob<OutboxMessage>
{
    public ProcessOutboxMessagesJob(ApplicationDbContext dbContext, IMediator mediator) 
        : base(dbContext, mediator) { }
}