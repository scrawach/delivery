using CSharpFunctionalExtensions;
using CSharpFunctionalExtensions.ValueTasks;
using DeliveryApp.Core.Domain.Model.OrderAggregate;
using DeliveryApp.Core.Ports;
using DeliveryApp.Core.Ports.Specifications;
using Microsoft.EntityFrameworkCore;
using Primitives;

namespace DeliveryApp.Infrastructure.Adapters.Postgres.Repositories;

public class OrderRepository : IOrderRepository
{
    private readonly ApplicationDbContext _dbContext;

    public OrderRepository(ApplicationDbContext dbContext) => 
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));

    public async Task<UnitResult<Error>> AddAsync(Order order) =>
        await order.ToResult(GeneralErrors.ValueIsRequired(nameof(order)))
            .TapIf(o => o.Status != null, o => _dbContext.Attach(o.Status))
            .Map(entity => _dbContext.Orders.AddAsync(entity));
    
    public UnitResult<Error> Update(Order order) =>
        order.ToResult(GeneralErrors.ValueIsRequired(nameof(order)))
            .TapIf(o => o.Status != null, o => _dbContext.Attach(o.Status))
            .Map(entity => _dbContext.Orders.Update(entity));

    public async Task<Result<Order, Error>> GetByIdAsync(Guid id) =>
        await _dbContext.Orders
            .Include(order => order.Status)
            .FirstOrDefaultAsync(order => order.Id == id)
            .ToResultAsync(GeneralErrors.NotFound(id));

    public Result<IEnumerable<Order>, Error> All(ISpecification<Order> match) =>
        match
            .ToResult(GeneralErrors.ValueIsRequired(nameof(match)))
            .Map(specification => _dbContext.Orders
                .Include(order => order.Status)
                .Where(specification.Expression)
                .AsEnumerable());
}