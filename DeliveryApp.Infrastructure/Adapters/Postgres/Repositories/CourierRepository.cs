using CSharpFunctionalExtensions;
using CSharpFunctionalExtensions.ValueTasks;
using DeliveryApp.Core.Domain.Model.CourierAggregate;
using DeliveryApp.Core.Ports;
using DeliveryApp.Core.Ports.Specifications;
using Microsoft.EntityFrameworkCore;
using Primitives;

namespace DeliveryApp.Infrastructure.Adapters.Postgres.Repositories;

public class CourierRepository : ICourierRepository
{
    private readonly ApplicationDbContext _dbContext;

    public CourierRepository(ApplicationDbContext dbContext) => 
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));

    public async Task<UnitResult<Error>> AddAsync(Courier courier) =>
        await courier.ToResult(GeneralErrors.ValueIsRequired(nameof(courier)))
            .TapIf(c => c.Status != null, c => _dbContext.Attach(c.Status))
            .TapIf(c => c.Transport != null, c => _dbContext.Attach(c.Transport))
            .Map(entity => _dbContext.Couriers.AddAsync(entity));

    public UnitResult<Error> Update(Courier courier) =>
        courier.ToResult(GeneralErrors.ValueIsRequired(nameof(courier)))
            .TapIf(c => c.Status != null, c => _dbContext.Attach(c.Status))
            .TapIf(c => c.Transport != null, c => _dbContext.Attach(c.Transport))
            .Map(entity => _dbContext.Couriers.Update(entity));

    public async Task<Result<Courier, Error>> GetByIdAsync(Guid id) =>
        await _dbContext.Couriers
            .Include(courier => courier.Status)
            .Include(courier => courier.Transport)
            .FirstOrDefaultAsync(courier => courier.Id == id)
            .ToResultAsync(GeneralErrors.NotFound(id));

    public Result<IEnumerable<Courier>, Error> All(ISpecification<Courier> match) =>
        match
            .ToResult(GeneralErrors.ValueIsRequired(nameof(match)))
            .Map(specification => _dbContext.Couriers
                .Include(courier => courier.Status)
                .Include(courier => courier.Transport)
                .Where(specification.Expression)
                .AsEnumerable());
}