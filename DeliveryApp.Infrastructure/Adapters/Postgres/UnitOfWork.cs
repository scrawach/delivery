using DeliveryApp.Infrastructure.Adapters.Postgres.Entities;
using Newtonsoft.Json;
using Primitives;

namespace DeliveryApp.Infrastructure.Adapters.Postgres;

public class UnitOfWork : IUnitOfWork, IDisposable
{
    private readonly ApplicationDbContext _dbContext;

    private bool _disposed;

    public UnitOfWork(ApplicationDbContext dbContext) => 
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
    {
        await SaveDomainEventsAsOutboxMessagesAsync(cancellationToken);
        await _dbContext.SaveChangesAsync(cancellationToken);
        
        return true;
    }

    public virtual void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            if (disposing) _dbContext.Dispose();
            _disposed = true;
        }
    }

    private async Task SaveDomainEventsAsOutboxMessagesAsync(CancellationToken cancellationToken = default) => 
        await _dbContext.Set<OutboxMessage>().AddRangeAsync(OutboxMessagesFromAggregates(), cancellationToken);

    private IEnumerable<OutboxMessage> OutboxMessagesFromAggregates() =>
        _dbContext.ChangeTracker
            .Entries<Aggregate>()
            .SelectMany(x =>
            {
                var events = x.Entity.GetDomainEvents(); 
                x.Entity.ClearDomainEvents(); 
                return events;
            })
            .Select(domainEvent => new OutboxMessage() 
            { 
                Id = domainEvent.EventId, 
                Type = domainEvent.GetType().ToString(), 
                Content = JsonConvert.SerializeObject(domainEvent, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                }), 
                OccuredOnUtc = DateTime.UtcNow 
            });
}