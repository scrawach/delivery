﻿using Confluent.Kafka;
using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.Model.OrderAggregate.DomainEvents;
using DeliveryApp.Core.Ports;
using Google.Protobuf;
using Newtonsoft.Json;
using Error = Primitives.Error;

namespace DeliveryApp.Infrastructure.Adapters.Kafka.OrderStatusChanged;

public sealed class Producer : IBusProducer
{
    private const string TopicName = "order.status.changed";

    private IProducer<string, string> _producer;
    
    public Producer(IProducer<string, string> producer)
    {
        _producer = producer ?? throw new ArgumentNullException(nameof(producer));
    }
    
    public async Task<UnitResult<Error>> Publish(OrderStatusChangedDomainEvent domainEvent, CancellationToken token = default)
    {
        var integrationEvent = domainEvent.ToIntegrationEvent();
        var message = BuildMessage(domainEvent.EventId, integrationEvent);
        await _producer.ProduceAsync(TopicName, message, token);
        return UnitResult.Success<Error>();
    }

    private static Message<string, string> BuildMessage(Guid key, IMessage integrationEvent) =>
        new()
        {
            Key = key.ToString(),
            Value = JsonConvert.SerializeObject(integrationEvent)
        };
}