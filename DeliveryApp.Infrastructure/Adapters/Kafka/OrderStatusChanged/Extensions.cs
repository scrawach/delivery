﻿using DeliveryApp.Core.Domain.Model.OrderAggregate.DomainEvents;
using OrderStatusChanged;

namespace DeliveryApp.Infrastructure.Adapters.Kafka.OrderStatusChanged;

public static class Extensions
{
    public static OrderStatusChangedIntegrationEvent ToIntegrationEvent(this OrderStatusChangedDomainEvent domainEvent) =>
        new()
        {
            OrderId = domainEvent.EventId.ToString(),
            OrderStatus = (OrderStatus) domainEvent.CurrentStatus.Id
        };
}