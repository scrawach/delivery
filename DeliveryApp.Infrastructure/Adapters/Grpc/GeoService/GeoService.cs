﻿using CSharpFunctionalExtensions;
using DeliveryApp.Core.Ports;
using GeoApp.Api;
using Grpc.Core;
using Primitives;
using Location = DeliveryApp.Core.Domain.Model.SharedKernel.Location;

namespace DeliveryApp.Infrastructure.Adapters.Grpc.GeoService;

public class GeoService : IGeoService
{
    private readonly Geo.GeoClient _client;
    
    public GeoService(Geo.GeoClient client) => 
        _client = client ?? throw new ArgumentNullException(nameof(client));

    public async Task<Result<Location, Error>> GetLocationFromStreet(string street, CancellationToken token = default)
    {
        try
        {
            var reply = await _client.GetGeolocationAsync(new GetGeolocationRequest { Street = street }, cancellationToken: token);
            return Location.Create(reply.Location.X, reply.Location.Y);
        }
        catch(RpcException exception) 
        {
            return Errors.RpcConnectionError(exception.Message);
        }
    }

    public static class Errors
    {
        public static Error RpcConnectionError(string message) => 
            new($"{nameof(GeoService)}.rpc.connection.error", message);
    }
}