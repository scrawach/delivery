
using System.Reflection;
using Api.Filters;
using Api.Formatters;
using Api.OpenApi;
using Confluent.Kafka;
using Confluent.Kafka.Options;
using CSharpFunctionalExtensions;
using DeliveryApp.Api.Adapters.BackgroundJobs;
using DeliveryApp.Api.Adapters.Kafka.BasketConfirmed;
using DeliveryApp.Core.Application.DomainEventHandlers;
using DeliveryApp.Core.Application.DomainEvents;
using DeliveryApp.Core.Application.UseCases.Commands.AssignOrderOnCourier;
using DeliveryApp.Core.Application.UseCases.Commands.CreateOrder;
using DeliveryApp.Core.Application.UseCases.Commands.MoveCouriers;
using DeliveryApp.Core.Application.UseCases.Queries.GetBusyCouriers;
using DeliveryApp.Core.Application.UseCases.Queries.GetCouriers;
using DeliveryApp.Core.Application.UseCases.Queries.GetCreatedAndAssignedOrders;
using DeliveryApp.Core.Domain.Model.OrderAggregate.DomainEvents;
using DeliveryApp.Core.Domain.Services;
using DeliveryApp.Core.Ports;
using DeliveryApp.Infrastructure.Adapters.Grpc.GeoService;
using DeliveryApp.Infrastructure.Adapters.Kafka.OrderStatusChanged;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using DeliveryApp.Infrastructure.Adapters.Postgres.BackgroundJobs;
using DeliveryApp.Infrastructure.Adapters.Postgres.Repositories;
using GeoApp.Api;
using Grpc.Core;
using Grpc.Net.Client.Configuration;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Primitives;
using Quartz;
using Error = Primitives.Error;

namespace DeliveryApp.Api.Extensions;

public static class ServicesExtensions
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services, string connectionString)
    {
        // Database
        services.Configure<DbContextOptionsBuilder>(options =>
        {
            options.UseNpgsql(connectionString,
                sqlOptions => { sqlOptions.MigrationsAssembly("DeliveryApp.Infrastructure"); });
            options.EnableSensitiveDataLogging();
        });
        
        services.AddDbContextFactory<ApplicationDbContext>();
        services.AddDbContext<ApplicationDbContext>();
        services.AddTransient<IUnitOfWork, UnitOfWork>();
        
        // Ports & Adapters
        services.AddTransient<ICourierRepository, CourierRepository>();
        services.AddTransient<IOrderRepository, OrderRepository>();
        
        // GeoService
        services.AddTransient<IGeoService, GeoService>();
        
        // Kafka
        services.AddTransient<IBusProducer, Producer>();
        
        return services;
    }

    public static IServiceCollection AddDomainServices(this IServiceCollection services)
    {
        services.AddTransient<IDispatchService, DispatchService>();
        services.AddTransient<ICouriersMoveService, CouriersMoveService>();
        
        return services;
    }

    public static IServiceCollection AddUseCases(this IServiceCollection services, string connectionString)
    {
        // MediatR
        services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblyContaining<Startup>());

        // Commands
        services.AddTransient<IRequestHandler<CreateOrderCommand, UnitResult<Error>>, CreateOrderHandler>();
        services.AddTransient<IRequestHandler<AssignOrderOnCourierCommand, UnitResult<Error>>, AssignOrderOnCourierHandler>();
        services.AddTransient<IRequestHandler<MoveCourierCommand, UnitResult<Error>>, MoveCourierHandler>();
        
        // Queries
        services.AddTransient<IRequestHandler<GetCreatedAndAssignedOrdersQuery, Result<GetCreatedAndAssignedOrdersResponse, Error>>>
        (
            _ => new GetCreatedAndAssignedOrdersHandler(connectionString)
        );

        services.AddTransient<IRequestHandler<GetBusyCouriersQuery, Result<GetBusyCouriersResponse, Error>>>
        (
            _ => new GetBusyCouriersHandler(connectionString)
        );
        
        services.AddTransient<IRequestHandler<GetCouriersQuery, Result<GetCouriersResponse, Error>>>
        (
            _ => new GetCouriersHandler(connectionString)
        );
        
        // Domain Events Handlers
        services.AddTransient<INotificationHandler<OrderStatusChangedDomainEvent>, OrderStatusChangedDomainEventHandler>();
        services.AddTransient<INotificationHandler<BasketConfirmedDomainEvent>, BasketConfirmedDomainEventHandler>();
        return services;
    }

    public static IServiceCollection AddBackgroundServices(this IServiceCollection services)
    {
        services.Configure<HostOptions>(options =>
        {
            options.BackgroundServiceExceptionBehavior = BackgroundServiceExceptionBehavior.Ignore;
            options.ShutdownTimeout = TimeSpan.FromSeconds(30);
        });
        
        services.AddHostedService<Consumer>();
        return services;
    }

    public static IServiceCollection AddBackgroundJobs(this IServiceCollection services)
    {
        services.AddQuartz(configure =>
        {
            var assignOrdersJobKey = new JobKey(nameof(AssignOrdersJob));
            var moveCouriersJobKey = new JobKey(nameof(MoveCouriersJob));
            var processOutboxJobKey = new JobKey(nameof(ProcessOutboxMessagesJob));
            var processInboxJobKey = new JobKey(nameof(ProcessInboxMessagesJob));

            configure
                .AddJob<AssignOrdersJob>(assignOrdersJobKey)
                .AddTrigger(trigger =>
                    trigger
                        .ForJob(assignOrdersJobKey)
                        .WithSimpleSchedule(schedule => schedule.WithIntervalInSeconds(1).RepeatForever()))
                
                .AddJob<MoveCouriersJob>(moveCouriersJobKey)
                .AddTrigger(trigger =>
                    trigger
                        .ForJob(moveCouriersJobKey)
                        .WithSimpleSchedule(schedule => schedule.WithIntervalInSeconds(2).RepeatForever()))
                
                .AddJob<ProcessOutboxMessagesJob>(processOutboxJobKey)
                .AddTrigger(trigger => 
                    trigger
                        .ForJob(processOutboxJobKey)
                        .WithSimpleSchedule(schedule => schedule.WithIntervalInSeconds(2).RepeatForever()))
                
                .AddJob<ProcessInboxMessagesJob>(processInboxJobKey)
                .AddTrigger(trigger => 
                    trigger
                        .ForJob(processInboxJobKey)
                        .WithSimpleSchedule(schedule => schedule.WithIntervalInSeconds(2).RepeatForever()));
        });

        services.AddQuartzHostedService();
        
        return services;
    }

    public static IServiceCollection AddSwagger(this IServiceCollection services)
    {
        services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("1.0.0", new OpenApiInfo
            {
                Title = "Delivery Service",
                Description = "Отвечает за учет курьеров, деспетчеризацию доставкуов, доставку",
                Contact = new OpenApiContact
                {
                    Name = "Kirill Vetchinkin",
                    Url = new Uri("https://microarch.ru"),
                    Email = "info@microarch.ru"
                }
            });
            options.CustomSchemaIds(type => type.FriendlyId(true));
            options.IncludeXmlComments($"{AppContext.BaseDirectory}{Path.DirectorySeparatorChar}{Assembly.GetEntryAssembly().GetName().Name}.xml");
            options.DocumentFilter<BasePathFilter>("");
            options.OperationFilter<GeneratePathParamsValidationFilter>();
        });
        services.AddSwaggerGenNewtonsoftSupport();
        
        return services;
    }

    public static IServiceCollection AddKafkaAdapter(this IServiceCollection services, string messageBrokerHost)
    {
        services.Configure<KafkaClientOptions>(options =>
        {
            options.Configure(new ConsumerConfig()
            {
                BootstrapServers = messageBrokerHost,
                GroupId = "DeliveryConsumerGroup",
                EnableAutoOffsetStore = false,
                EnableAutoCommit = true,
                AutoOffsetReset = AutoOffsetReset.Earliest,
                EnablePartitionEof = true
            });

            options.Configure(new ProducerConfig()
            {
                BootstrapServers = messageBrokerHost
            });
        });
        
        services.AddKafkaClient();
        return services;
    }

    public static IServiceCollection AddHttpAdapter(this IServiceCollection services)
    {
        services.AddControllers(options =>
            {
                options.InputFormatters.Insert(0, new InputFormatterStream());
            })
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.Converters.Add(new StringEnumConverter
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                });
            });
        
        return services;
    }
    
    public static IServiceCollection AddGrpcAdapter(this IServiceCollection services, string geoServiceUrl)
    {
        services
            .AddGrpcClient<Geo.GeoClient>(options => 
            { 
                options.Address = new Uri(geoServiceUrl); 
            })
            .ConfigureChannel(options => 
            { 
                options.HttpHandler =  new SocketsHttpHandler
                {
                    PooledConnectionIdleTimeout = Timeout.InfiniteTimeSpan,
                    KeepAlivePingDelay = TimeSpan.FromSeconds(60),
                    KeepAlivePingTimeout = TimeSpan.FromSeconds(30),
                    EnableMultipleHttp2Connections = true
                };

                options.ServiceConfig = new ServiceConfig
                {
                    MethodConfigs =
                    {
                        new MethodConfig()
                        {
                            Names = { MethodName.Default },
                            RetryPolicy = new RetryPolicy
                            {
                                MaxAttempts = 5,
                                InitialBackoff = TimeSpan.FromSeconds(1),
                                MaxBackoff = TimeSpan.FromSeconds(5),
                                BackoffMultiplier = 1.5,
                                RetryableStatusCodes = { StatusCode.Unavailable }
                            }
                        }
                    }
                };
            });
        
        return services;
    }
}