﻿using Api.Controllers;
using DeliveryApp.Core.Application.UseCases.Commands.CreateOrder;
using DeliveryApp.Core.Application.UseCases.Queries.GetBusyCouriers;
using DeliveryApp.Core.Application.UseCases.Queries.GetCouriers;
using DeliveryApp.Core.Application.UseCases.Queries.GetCreatedAndAssignedOrders;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Courier = Api.Models.Courier;
using Location = Api.Models.Location;
using Order = Api.Models.Order;

namespace DeliveryApp.Api.Adapters.HTTP;

public class DeliveryController : DefaultApiController
{
    private readonly IMediator _mediator;

    public DeliveryController(IMediator mediator) => 
        _mediator = mediator;

    public override async Task<IActionResult> CreateOrder()
    {
        var command = new CreateOrderCommand(Guid.NewGuid(), "Empty");
        var response = await _mediator.Send(command);
        
        if (response.IsSuccess)
            return Ok();
        
        return Conflict();
    }

    public override async Task<IActionResult> GetCouriers()
    {
        var command = new GetCouriersQuery();
        var response = await _mediator.Send(command);

        if (response.IsFailure)
            return NotFound();

        var model = response.Value.Couriers.Select(courier => new Courier()
        {
            Id = courier.Id,
            Location = new Location() { X = courier.Location.X, Y = courier.Location.Y },
            Name = courier.Name
        });
        
        return Ok(model);
    }

    public override async Task<IActionResult> GetOrders()
    {
        var command = new GetCreatedAndAssignedOrdersQuery();
        var response = await _mediator.Send(command);

        if (response.IsFailure)
            return NotFound();

        var model = response.Value.Orders.Select(order => new Order()
        {
            Id = order.Id,
            Location = new Location() { X = order.Location.X, Y = order.Location.Y }
        });
        
        return Ok(model);
    }
}