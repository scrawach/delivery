﻿using BasketConfirmed;
using Confluent.Kafka;
using DeliveryApp.Core.Application.DomainEvents;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using DeliveryApp.Infrastructure.Adapters.Postgres.Entities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DeliveryApp.Api.Adapters.Kafka.BasketConfirmed;

public class Consumer : BackgroundService
{
    private const string TopicName = "basket.confirmed";

    private readonly IConsumer<string, string> _consumer;
    private readonly IDbContextFactory<ApplicationDbContext> _contextFactory;
    
    public Consumer(IConsumer<string, string> consumer, IDbContextFactory<ApplicationDbContext> contextFactory)
    {
        _consumer = consumer ?? throw new ArgumentNullException(nameof(consumer));
        _contextFactory = contextFactory ?? throw new ArgumentNullException(nameof(contextFactory));
    }
    
    protected override async Task ExecuteAsync(CancellationToken cancellationToken)
    {
        _consumer.Subscribe(TopicName);
        
        try
        {
            await ExecuteLoopAsync(cancellationToken);
        }
        catch (OperationCanceledException)
        {
            _consumer.Close();
        }
    }

    private async Task ExecuteLoopAsync(CancellationToken token)
    {
        while (!token.IsCancellationRequested)
        {
            await Task.Delay(TimeSpan.FromSeconds(1), token);
            var consumeResult = _consumer.Consume(token);
                
            if (consumeResult.IsPartitionEOF) 
                continue;

            var basketConfirmedEvent = JsonConvert.DeserializeObject<BasketConfirmedIntegrationEvent>(consumeResult.Message.Value);
            await HandleEvent(basketConfirmedEvent, token);
                
            try
            {
                _consumer.StoreOffset(consumeResult);
            }
            catch (KafkaException e)
            {
                Console.WriteLine($"Store offset error: {e.Error.Reason}");
            }
        }
    }

    private async Task HandleEvent(BasketConfirmedIntegrationEvent integrationEvent, CancellationToken token)
    {
        await using var dbContext = await _contextFactory.CreateDbContextAsync(token);

        var domainEvent = new BasketConfirmedDomainEvent(Guid.Parse(integrationEvent.BasketId), integrationEvent.Address.Street);
        
        var inboxMessage = new InboxMessage()
        {
            Id = domainEvent.EventId,
            Type = domainEvent.GetType().ToString(),
            Content = JsonConvert.SerializeObject(domainEvent,
                new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.All }),
            OccuredOnUtc = DateTime.UtcNow
        };
        
        var find = await dbContext
            .InboxMessages
            .FindAsync([inboxMessage.Id], cancellationToken: token);

        if (find != null)
            return;

        await dbContext.InboxMessages.AddAsync(inboxMessage, token);
        await dbContext.SaveChangesAsync(token);
    }
}