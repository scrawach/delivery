using DeliveryApp.Api.Extensions;

namespace DeliveryApp.Api;

public class Startup
{
    public Startup()
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddEnvironmentVariables();
        var configuration = builder.Build();
        Configuration = configuration;
    }

    /// <summary>
    ///     Конфигурация
    /// </summary>
    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
        // Health Checks
        services.AddHealthChecks();

        // Cors
        services.AddCors(options =>
        {
            options.AddDefaultPolicy(
                policy =>
                {
                    policy.AllowAnyOrigin(); // Не делайте так в проде!
                    policy.AllowAnyHeader();
                    policy.AllowAnyMethod();
                });
        });

        // Configuration
        services.Configure<Settings>(options => Configuration.Bind(options));
        var connectionString = Configuration["CONNECTION_STRING"];
        var geoServiceGrpcHost = Configuration["GEO_SERVICE_GRPC_HOST"];
        var messageBrokerHost = Configuration["MESSAGE_BROKER_HOST"];
        
        services.AddInfrastructure(connectionString);
        services.AddDomainServices();
        services.AddUseCases(connectionString);
        services.AddBackgroundJobs();
        services.AddHttpAdapter();
        services.AddGrpcAdapter(geoServiceGrpcHost);
        services.AddKafkaAdapter(messageBrokerHost);
        services.AddSwagger();
        services.AddBackgroundServices();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
            app.UseDeveloperExceptionPage();
        else
            app.UseHsts();

        app.UseHealthChecks("/health");
        app.UseRouting();
        app.UseDefaultFiles();
        app.UseStaticFiles();

        app.UseCors(builder => builder
            .AllowAnyHeader()
            .AllowAnyMethod()
            .AllowAnyOrigin()
        );
        
        app.UseSwagger(c =>
        {
            c.RouteTemplate = "openapi/{documentName}/openapi.json";
        });
        app.UseSwaggerUI(options =>
        {
            options.RoutePrefix = "openapi";
            options.SwaggerEndpoint("/openapi/1.0.0/openapi.json", "Swagger Delivery Service");
            options.RoutePrefix = string.Empty;
            options.SwaggerEndpoint("/openapi-original.json", "Swagger Delivery Service");
        });

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}