using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DeliveryApp.Core.Domain.Model.SharedKernel;
using FluentAssertions;
using Primitives;
using Xunit;

namespace DeliveryApp.UnitTests.Core.Domain.Model.SharedKernel;

public class PathTests
{
    [Fact]
    public void WhenCreated_WithFirstInvalidParameters_ThenShouldReturnFailureResult_WithErrorMessage_AboutRequiredParameters()
    {
        // act
        var path = Path.Create(null, Location.Min);
        
        // assert
        path.IsFailure.Should().BeTrue();
        path.Error.Should().Be(GeneralErrors.ValueIsRequired("from"));
    }
    
    [Fact]
    public void WhenCreated_WithSecondInvalidParameters_ThenShouldReturnFailureResult_WithErrorMessage_AboutRequiredParameters()
    {
        // act
        var path = Path.Create(Location.Max, null);
        
        // assert
        path.IsFailure.Should().BeTrue();
        path.Error.Should().Be(GeneralErrors.ValueIsRequired("to"));
    }

    [Fact]
    public void WhenCreated_WithValidParameters_ThenShouldReturnSuccessResult_AndContainsParameters()
    {
        // act
        var path = Path.Create(Location.Min, Location.Max);
        
        // assert
        path.IsSuccess.Should().BeTrue();
        path.Value.Start.Should().Be(Location.Min);
        path.Value.Target.Should().Be(Location.Max);
    }

    [Fact]
    public void WhenGetPoints_BetweenNearLocations_ThenShouldReturnOnlyTargetLocation()
    {
        // arrange
        var target = Location.Create(1, 2).Value;
        var path = Path.Create(Location.Min, target);
        
        // act
        var points = path.Value.GetPoints();

        // assert
        points.IsSuccess.Should().BeTrue();
        points.Value.Should().BeEquivalentTo(new []{ target });
    }

    [Theory]
    [ClassData(typeof(PointsBetweenLocationTest))]
    public void WhenGetPoints_ThenShouldReturnLocationsBetweenTwoLocation(Location start, Location target, Location[] expectedPoints)
    {
        // arrange
        var path = Path.Create(start, target);
        
        // act
        var points = path.Value.GetPoints();

        // assert
        points.IsSuccess.Should().BeTrue();
        points.Value.Should().BeEquivalentTo(expectedPoints);
    }
    
    private class PointsBetweenLocationTest : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return [CreateLocation(1, 1), CreateLocation(2, 2), Locations((2, 1), (2, 2))];
            yield return [CreateLocation(1, 1), CreateLocation(3, 2), Locations((2, 1), (2, 2), (3, 2))];
            yield return [CreateLocation(1, 1), CreateLocation(3, 4), Locations((2, 1), (2, 2), (3, 2), (3, 3), (3, 4))];
        }
        
        private Location CreateLocation(int x, int y) => 
            Location.Create(x, y).Value;

        private Location[] Locations(params (int x, int y)[] positions) => 
            positions.
                Select(position => Location.Create(position.x, position.y).Value)
                .ToArray();

        IEnumerator IEnumerable.GetEnumerator() => 
            GetEnumerator();
        
    }
}