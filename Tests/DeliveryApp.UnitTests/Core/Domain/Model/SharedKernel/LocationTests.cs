using System;
using System.Collections;
using System.Collections.Generic;
using DeliveryApp.Core.Domain.Model.SharedKernel;
using FluentAssertions;
using Primitives;
using Xunit;

namespace DeliveryApp.UnitTests.Core.Domain.Model.SharedKernel;

public class LocationTests
{
    [Fact]
    public void WhenCreateLocationFromFactoryMethod_WithValidCoordinates_ThenShouldReturnSuccessResult()
    {
        // act
        var result = Location.Create(1, 1);

        // assert
        result.IsSuccess.Should().BeTrue();
    }

    [Theory]
    [InlineData(1, 1)]
    [InlineData(5, 5)]
    [InlineData(10, 10)]
    public void WhenCreateLocationFromFactoryMethod_WithValidCoordinates_ThenShouldPropagateCoordinatesInsideLocation(int x, int y)
    {
        // act
        var result = Location.Create(x, y);
        
        // assert
        var location = result.Value;
        location.X.Should().Be(x);
        location.Y.Should().Be(y);
    }

    [Theory]
    [InlineData(0)]
    [InlineData(11)]
    public void WhenCreateLocationFromFactoryMethod_WithInvalidXCoordinates_ThenShouldReturnResultWithInvalidValueError(int value)
    {
        // act
        var result = Location.Create(value, 5);
        
        // assert
        result.IsSuccess.Should().BeFalse();
        result.Error.Should().Be(GeneralErrors.ValueIsInvalid("x"));
    }
    
    [Theory]
    [InlineData(0)]
    [InlineData(11)]
    public void WhenCreateLocationFromFactoryMethod_WithInvalidYCoordinates_ThenShouldReturnResultWithInvalidValueError(int value)
    {
        // act
        var result = Location.Create(5, value);
        
        // assert
        result.IsSuccess.Should().BeFalse();
        result.Error.Should().Be(GeneralErrors.ValueIsInvalid("y"));
    }
    
    [Fact]
    public void WhenCreateLocationFromFactoryMethod_WithInvalidCoordinates_ThenShouldReturnResultWithInvalidValueError_AboutXCoordinate()
    {
        // act
        var result = Location.Create(11, 11);
        
        // assert
        result.IsSuccess.Should().BeFalse();
        result.Error.Should().Be(GeneralErrors.ValueIsInvalid("x"));
    }

    [Fact]
    public void WhenCreateLocationFromRandom_ThenShouldReturnValidLocation_AndSuccessResult()
    {
        // assign
        var random = new Random(0);
        
        // act
        var result = Location.Random(random);
        
        // assert
        result.IsSuccess.Should().BeTrue();
    }

    [Fact]
    public void WhenLocationsHaveSameCoordinates_ThenTheyShouldBeEquals()
    {
        // assign
        var first = Location.Create(5, 5);
        var second = Location.Create(5, 5);
        
        // act
        var isEquals = first.Value == second.Value;

        // assert
        isEquals.Should().BeTrue();
    }

    [Fact]
    public void WhenLocationsHaveDifferentCoordinates_ThenTheyShouldBeNotEquals()
    {
        // assign
        var first = Location.Create(4, 5);
        var second = Location.Create(5, 5);
        
        // act
        var isEquals = first.Value == second.Value;

        // assert
        isEquals.Should().BeFalse();
    }

    [Theory]
    [ClassData(typeof(CalculateDistanceTestData))]
    [ClassData(typeof(RandomCalculateDistanceTestData))]
    public void WhenCalculateDistanceBetweenLocation_ThenShouldReturnValidDistance(Location start, Location target, int expected)
    {
        // act
        var distance = start.DistanceTo(target);
        
        // assert
        distance.Should().Be(expected);
    }

    private class CalculateDistanceTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return Test((1, 1), (1, 1), 0);
            yield return Test((2, 1), (1, 1), 1);
            yield return Test((1, 1), (2, 1), 1);
            yield return Test((3, 1), (1, 1), 2);
            yield return Test((2, 2), (1, 1), 2);
            yield return Test((5, 5), (10, 10), 10);
            yield return Test((10, 10), (5, 5), 10);
            yield return Test((1, 1), (10, 10), 18);
            yield return Test((10, 10), (1, 1), 18);
        }

        private object[] Test((int x, int y) start, (int x, int y) target, int distance) =>
            [
                Location.Create(start.x, start.y).Value, 
                Location.Create(target.x, target.y).Value, 
                distance
            ];


        IEnumerator IEnumerable.GetEnumerator() => 
            GetEnumerator();
    }

    private class RandomCalculateDistanceTestData : IEnumerable<object[]>
    {
        private const int AmountOfIterations = 100;
        private const int RandomSeed = 0;
        
        public IEnumerator<object[]> GetEnumerator()
        {
            var random = new Random(RandomSeed);
            for (var i = 0; i < AmountOfIterations; i++)
                yield return RandomTestCase(random);
        }

        private object[] RandomTestCase(Random random)
        {
            var first = Location.Random(random).Value;
            var second = Location.Random(random).Value;
            var expectedDistance = Math.Abs(second.X - first.X) + Math.Abs(second.Y - first.Y);
            return [first, second, expectedDistance];
        }

        IEnumerator IEnumerable.GetEnumerator() => 
            GetEnumerator();
    }
}