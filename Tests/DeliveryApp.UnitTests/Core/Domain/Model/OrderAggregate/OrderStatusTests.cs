using System.Collections;
using System.Collections.Generic;
using DeliveryApp.Core.Domain.Model.OrderAggregate;
using FluentAssertions;
using Xunit;

namespace DeliveryApp.UnitTests.Core.Domain.Model.OrderAggregate;

public class OrderStatusTests
{
    [Theory]
    [ClassData(typeof(ExistingOrderStatusNames))]
    public void WhenCreateFromName_WithExistingName_ThenShouldReturnSuccessResult_WithSameName_InLowerCase(string name)
    {
        // act
        var status = OrderStatus.FromName(name);
        
        // assert
        status.IsSuccess.Should().BeTrue();
        status.Value.Name.Should().Be(name.ToLowerInvariant());
    }

    [Theory]
    [ClassData(typeof(NotExistingOrderStatusNames))]
    public void WhenCreateFromName_WithNotExistingName_ThenShouldReturnFailureResult(string name)
    {
        // act
        var status = OrderStatus.FromName(name);
        
        // assert
        status.IsFailure.Should().BeTrue();
    }

    [Theory]
    [ClassData(typeof(NotExistingOrderStatusNames))]
    public void WhenCreateFromName_WithNotExistingName_ThenShouldReturnFailureResult_WithExpectedErrorCode(string name)
    {
        // act
        var status = OrderStatus.FromName(name);
        
        // assert
        status.Error.Code.Should().Be($"{nameof(OrderStatus).ToLowerInvariant()}.name.not.found");
    }

    [Theory]
    [ClassData(typeof(ExistingOrderStatusIds))]
    public void WhenCreateFromId_WithExistingId_ThenShouldReturnSuccessResult_WithSameId(int id)
    {
        // act
        var status = OrderStatus.FromId(id);
        
        // assert
        status.IsSuccess.Should().BeTrue();
        status.Value.Id.Should().Be(id);
    }
    
    [Theory]
    [ClassData(typeof(NotExistingOrderStatusIds))]
    public void WhenCreateFromId_WithNotExistingId_ThenShouldReturnFailureResult(int id)
    {
        // act
        var status = OrderStatus.FromId(id);
        
        // assert
        status.IsFailure.Should().BeTrue();
    }
    
    [Theory]
    [ClassData(typeof(NotExistingOrderStatusIds))]
    public void WhenCreateFromId_WithNotExistingId_ThenShouldReturnFailureResult_WithExpectedErrorCode(int id)
    {
        // act
        var status = OrderStatus.FromId(id);
        
        // assert
        status.Error.Code.Should().Be($"{nameof(OrderStatus).ToLowerInvariant()}.id.not.found");
    }

    [Fact]
    public void WhenGetListOfStatuses_ThenShouldReturnNotEmptyCollection()
    {
        // act
        var allStatuses = OrderStatus.List();
        
        // assert
        allStatuses.Should().NotBeEmpty();
    }
    
    private class ExistingOrderStatusNames : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return ["Created"];
            yield return ["Assigned"];
            yield return ["Completed"];
        }
        
        IEnumerator IEnumerable.GetEnumerator() => 
            GetEnumerator();
    }
    
    private class NotExistingOrderStatusNames : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return ["Failed"];
            yield return ["Processed"];
            yield return [""];
        }
        
        IEnumerator IEnumerable.GetEnumerator() => 
            GetEnumerator();
    }
   
    private class ExistingOrderStatusIds : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return [1];
            yield return [2];
            yield return [3];
        }
        
        IEnumerator IEnumerable.GetEnumerator() => 
            GetEnumerator();
    }
    
    private class NotExistingOrderStatusIds : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return [0];
            yield return [4];
            yield return [5];
        }
        
        IEnumerator IEnumerable.GetEnumerator() => 
            GetEnumerator();
    }
}