using System;
using System.Linq;
using DeliveryApp.Core.Domain.Model.CourierAggregate;
using DeliveryApp.Core.Domain.Model.OrderAggregate;
using DeliveryApp.Core.Domain.Model.OrderAggregate.DomainEvents;
using DeliveryApp.Core.Domain.Model.SharedKernel;
using FluentAssertions;
using Primitives;
using Xunit;

namespace DeliveryApp.UnitTests.Core.Domain.Model.OrderAggregate;

public class OrderTests
{
    [Fact]
    public void WhenCreated_ThenShouldHaveCreatedStatus()
    {
        // act
        var order = Order.Create(Guid.NewGuid(), Location.Min);
        
        // assert
        order.IsSuccess.Should().BeTrue();
        order.Value.Status.Should().Be(OrderStatus.Created);
    }

    [Fact]
    public void WhenCreated_WithValidParameters_ThenShouldHaveThisParameters()
    {
        // act
        var guid = Guid.NewGuid();
        var order = Order.Create(guid, Location.Max);
        
        // assert
        order.IsSuccess.Should().BeTrue();
        order.Value.Id.Should().Be(guid);
        order.Value.Location.Should().Be(Location.Max);
    }

    [Fact]
    public void WhenCreated_WithEmptyGuid_ThenShouldReturnFailureResult_WithErrorMessageAboutRequiredId()
    {
        // act
        var order = Order.Create(Guid.Empty, Location.Min);
        
        // assert
        order.IsFailure.Should().BeTrue();
        order.Error.Should().Be(GeneralErrors.ValueIsRequired("id"));
    }

    [Fact]
    public void WhenCreated_WithNullLocation_ThenShouldReturnFailureResult_WithErrorMessageAboutRequiredLocation()
    {
        // act
        var order = Order.Create(Guid.NewGuid(), null);
        
        // assert
        order.IsFailure.Should().BeTrue();
        order.Error.Should().Be(GeneralErrors.ValueIsRequired("location"));
    }

    [Fact]
    public void WhenAssignOnCourier_WhenShouldUpdateOrderStatusToAssigned_AndAddedCourierId()
    {
        // arrange
        var order = Order.Create(Guid.NewGuid(), Location.Min).Value;
        var courier = Courier.Create("bot", Transport.Pedestrian, Location.Max).Value;

        // act
        var result = order.Assign(courier);
        
        // assert
        result.IsSuccess.Should().BeTrue();
        order.CourierId.Should().Be(courier.Id);
        order.Status.Should().Be(OrderStatus.Assigned);
    }

    [Fact]
    public void WhenCompleteNotAssignedOrder_ThenShouldReturnFailure_WithErrorCode_AboutOrderNotAssigned()
    {
        // assert
        var order = Order.Create(Guid.NewGuid(), Location.Min);
        
        // act
        var result = order.Value.Complete();
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Code.Should().Be($"{nameof(Order).ToLowerInvariant()}.not.assigned");
    }

    [Fact]
    public void WhenOrderAssign_ThenShouldRaiseDomainEvent_AboutOrderStatusChangedFromCreatedToAssigned()
    {
        // assert
        var order = Order.Create(Guid.NewGuid(), Location.Min).Value;
        var courier = Courier.Create("bot", Transport.Bicycle, Location.Min).Value;

        // act
        var result = order.Assign(courier);
        var events = order.GetDomainEvents();
        
        // assert
        result.IsSuccess.Should().BeTrue();
        events.Should().ContainSingle();
        events.First().Should().BeOfType<OrderStatusChangedDomainEvent>();
        events.First().As<OrderStatusChangedDomainEvent>().PreviousStatus.Should().Be(OrderStatus.Created);
        events.First().As<OrderStatusChangedDomainEvent>().CurrentStatus.Should().Be(OrderStatus.Assigned);
    }
    
    [Fact]
    public void WhenOrderComplete_ThenShouldRaiseDomainEvent_AboutOrderStatusChangedFromAssignedToCompleted()
    {
        // assert
        var order = Order.Create(Guid.NewGuid(), Location.Min).Value;
        var courier = Courier.Create("bot", Transport.Bicycle, Location.Min).Value;
        order.Assign(courier);
        order.ClearDomainEvents();

        // act
        var result = order.Complete();
        var events = order.GetDomainEvents();
        
        // assert
        result.IsSuccess.Should().BeTrue();
        events.Should().ContainSingle();
        events.First().Should().BeOfType<OrderStatusChangedDomainEvent>();
        events.First().As<OrderStatusChangedDomainEvent>().PreviousStatus.Should().Be(OrderStatus.Assigned);
        events.First().As<OrderStatusChangedDomainEvent>().CurrentStatus.Should().Be(OrderStatus.Completed);
    }
}