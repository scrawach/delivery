using System.Collections;
using System.Collections.Generic;
using DeliveryApp.Core.Domain.Model.CourierAggregate;
using FluentAssertions;
using Xunit;

namespace DeliveryApp.UnitTests.Core.Domain.Model.CourierAggregate;

public class TransportTests
{
    [Theory]
    [ClassData(typeof(ExistingOrderStatusNames))]
    public void WhenCreateFromName_WithExistingName_ThenShouldReturnSuccessResult_WithSameName_InLowerCase(string name)
    {
        // act
        var status = Transport.FromName(name);
        
        // assert
        status.IsSuccess.Should().BeTrue();
        status.Value.Name.Should().Be(name.ToLowerInvariant());
    }

    [Theory]
    [ClassData(typeof(NotExistingOrderStatusNames))]
    public void WhenCreateFromName_WithNotExistingName_ThenShouldReturnFailureResult(string name)
    {
        // act
        var status = Transport.FromName(name);
        
        // assert
        status.IsFailure.Should().BeTrue();
    }

    [Theory]
    [ClassData(typeof(NotExistingOrderStatusNames))]
    public void WhenCreateFromName_WithNotExistingName_ThenShouldReturnFailureResult_WithExpectedErrorCode(string name)
    {
        // act
        var status = Transport.FromName(name);
        
        // assert
        status.Error.Code.Should().Be($"{nameof(Transport).ToLowerInvariant()}.name.not.found");
    }

    [Theory]
    [ClassData(typeof(ExistingOrderStatusIds))]
    public void WhenCreateFromId_WithExistingId_ThenShouldReturnSuccessResult_WithSameId(int id)
    {
        // act
        var status = Transport.FromId(id);
        
        // assert
        status.IsSuccess.Should().BeTrue();
        status.Value.Id.Should().Be(id);
    }
    
    [Theory]
    [ClassData(typeof(NotExistingOrderStatusIds))]
    public void WhenCreateFromId_WithNotExistingId_ThenShouldReturnFailureResult(int id)
    {
        // act
        var status = Transport.FromId(id);
        
        // assert
        status.IsFailure.Should().BeTrue();
    }
    
    [Theory]
    [ClassData(typeof(NotExistingOrderStatusIds))]
    public void WhenCreateFromId_WithNotExistingId_ThenShouldReturnFailureResult_WithExpectedErrorCode(int id)
    {
        // act
        var status = Transport.FromId(id);
        
        // assert
        status.Error.Code.Should().Be($"{nameof(Transport).ToLowerInvariant()}.id.not.found");
    }

    [Fact]
    public void WhenGetListOfStatuses_ThenShouldReturnNotEmptyCollection()
    {
        // act
        var allStatuses = Transport.List();
        
        // assert
        allStatuses.Should().NotBeEmpty();
    }

    [Fact]
    public void PedestrianShouldContainsCorrectSpeed()
    {
        // act
        var pedestrian = Transport.Pedestrian;
        
        // assert
        pedestrian.Speed.Should().Be(1);
    }
    
    [Fact]
    public void BicycleShouldContainsCorrectSpeed()
    {
        // act
        var bicycle = Transport.Bicycle;
        
        // assert
        bicycle.Speed.Should().Be(2);
    }
    
    [Fact]
    public void CarShouldContainsCorrectSpeed()
    {
        // act
        var car = Transport.Car;
        
        // assert
        car.Speed.Should().Be(3);
    }

    [Fact]
    public void CarShouldBeFaster_ThenOtherTransport()
    {
        // arrange
        var pedestrian = Transport.Pedestrian;
        var bicycle = Transport.Bicycle;
        var car = Transport.Car;
        
        // act
        var result = car.Speed > bicycle.Speed && car.Speed > pedestrian.Speed;
        
        // assert
        result.Should().BeTrue();
    }

    [Fact]
    public void BicycleShouldBeFaster_ThenPedestrianTransport()
    {
        // arrange
        var pedestrian = Transport.Pedestrian;
        var bicycle = Transport.Bicycle;
        
        // act
        var result = bicycle.Speed > pedestrian.Speed;
        
        // assert
        result.Should().BeTrue();
    }
    
    private class ExistingOrderStatusNames : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return ["Pedestrian"];
            yield return ["Bicycle"];
            yield return ["Car"];
        }
        
        IEnumerator IEnumerable.GetEnumerator() => 
            GetEnumerator();
    }
    
    private class NotExistingOrderStatusNames : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return ["None"];
            yield return ["Legs"];
            yield return [""];
        }
        
        IEnumerator IEnumerable.GetEnumerator() => 
            GetEnumerator();
    }
   
    private class ExistingOrderStatusIds : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return [1];
            yield return [2];
            yield return [3];
        }
        
        IEnumerator IEnumerable.GetEnumerator() => 
            GetEnumerator();
    }
    
    private class NotExistingOrderStatusIds : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return [0];
            yield return [4];
            yield return [5];
        }
        
        IEnumerator IEnumerable.GetEnumerator() => 
            GetEnumerator();
    }
}