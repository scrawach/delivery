using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DeliveryApp.Core.Domain.Model.CourierAggregate;
using DeliveryApp.Core.Domain.Model.SharedKernel;
using FluentAssertions;
using Primitives;
using Xunit;

namespace DeliveryApp.UnitTests.Core.Domain.Model.CourierAggregate;

public class CourierTests
{
    private const string CourierName = "bot";
    
    [Fact]
    public void WhenCreated_WithValidParameters_ThenShouldHaveThisParameters()
    {
        // arrange
        var guid = Guid.NewGuid();
        
        // act
        var courier = Courier.Create(guid, CourierName, Transport.Pedestrian, Location.Min).Value;
        
        // assert
        courier.Id.Should().Be(guid);
        courier.Name.Should().Be(CourierName);
        courier.Transport.Should().Be(Transport.Pedestrian);
        courier.Location.Should().Be(Location.Min);
    }
    
    [Fact]
    public void WhenCreated_WithEmptyName_ThenShouldReturnFailureResult_WithErrorMessageAboutNameRequired()
    {
        // act
        var courier = Courier.Create(string.Empty, Transport.Pedestrian, Location.Min);
        
        // assert
        courier.IsFailure.Should().BeTrue();
        courier.Error.Should().Be(GeneralErrors.ValueIsRequired("name"));
    }

    [Fact]
    public void WhenCreated_WithEmptyTransport_ThenShouldReturnFailureResult_WithErrorMessageAboutTransportRequired()
    {
        // act
        var courier = Courier.Create(CourierName, null, Location.Min);
        
        // assert
        courier.IsFailure.Should().BeTrue();
        courier.Error.Should().Be(GeneralErrors.ValueIsRequired("transport"));
    }
    
    [Fact]
    public void WhenCreated_WithNullLocation_ThenShouldReturnFailureResult_WithErrorMessageAboutLocationRequired()
    {
        // act
        var courier = Courier.Create(CourierName, Transport.Pedestrian, null);
        
        // assert
        courier.IsFailure.Should().BeTrue();
        courier.Error.Should().Be(GeneralErrors.ValueIsRequired("location"));
    }
    
    [Fact]
    public void WhenCreated_WithEmptyGuid_ThenShouldReturnFailureResult_WithErrorMessageAboutGuidRequired()
    {
        // act
        var courier = Courier.Create(Guid.Empty, CourierName, Transport.Pedestrian, Location.Min);
        
        // assert
        courier.IsFailure.Should().BeTrue();
        courier.Error.Should().Be(GeneralErrors.ValueIsRequired("guid"));
    }
    
    [Fact]
    public void WhenCreated_ThenShouldContainsFreeStatus()
    {
        // act
        var courier = Courier.Create(CourierName, Transport.Pedestrian, Location.Min);
        
        // assert
        courier.IsSuccess.Should().BeTrue();
        courier.Value.Status.Should().Be(CourierStatus.Free);
    }

    [Fact]
    public void WhenSetBusy_WhenFree_ThenShouldUpdateCourierStatus_ToBusy()
    {
        // arrange
        var courier = Courier.Create(CourierName, Transport.Pedestrian, Location.Min).Value;
        
        // act
        var result = courier.SetBusy();

        // assert
        result.IsSuccess.Should().BeTrue();
        courier.Status.Should().Be(CourierStatus.Busy);
    }
    
    [Fact]
    public void WhenSetFree_WhenBusy_ThenShouldUpdateCourierStatus_ToFree()
    {
        // arrange
        var courier = Courier.Create(CourierName, Transport.Pedestrian, Location.Min).Value;
        
        // act
        courier.SetBusy();
        var result = courier.SetFree();

        // assert
        result.IsSuccess.Should().BeTrue();
        courier.Status.Should().Be(CourierStatus.Free);
    }

    [Fact]
    public void WhenSetBusy_AndCourierAlreadyBusy_ThenShouldReturnFailureResult_WithConcreteErrorCode()
    {
        // arrange
        var courier = Courier.Create(CourierName, Transport.Pedestrian, Location.Min).Value;
        
        // act
        courier.SetBusy();
        var result = courier.SetBusy();
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Code.Should().Be($"{nameof(Courier).ToLowerInvariant()}.already.busy");
    }
    
    [Fact]
    public void WhenSetFree_AndCourierAlreadyFree_ThenShouldReturnFailureResult_WithConcreteErrorCode()
    {
        // arrange
        var courier = Courier.Create(CourierName, Transport.Pedestrian, Location.Min).Value;
        
        // act
        var result = courier.SetFree();
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Code.Should().Be($"{nameof(Courier).ToLowerInvariant()}.already.free");
    }

    [Theory]
    [ClassData(typeof(TimeToPointTestData))]
    public void WhenCalculateTimeToPoint_WithValidTransport_ThenShouldReturnSuccessResult(Location start,
        Location target, Transport transport, double expectedTime)
    {
        // arrange
        var courier = Courier.Create(CourierName, transport, start).Value;
        
        // act
        var totalTime = courier.CalculateTimeToPoint(target);
        
        // assert
        totalTime.IsSuccess.Should().BeTrue();
        totalTime.Value.Should().Be(expectedTime);
    }

    [Fact]
    public void WhenMoveToTarget_WithSpeedMoreThanDistance_ThenShouldStoppedOnTarget()
    {
        // arrange
        var start = Location.Create(1, 1).Value;
        var target = Location.Create(2, 1).Value;
        var courier = Courier.Create(CourierName, Transport.Car, start).Value;
        
        // act
        var result = courier.Move(target);
        
        // assert
        result.IsSuccess.Should().BeTrue();
        courier.Location.Should().Be(target);
    }
    
    [Fact]
    public void WhenMoveToTarget_WithReverseMovement_ThenShouldReturnSuccessResult()
    {
        // arrange
        var start = Location.Create(2, 1).Value;
        var target = Location.Create(1, 1).Value;
        var courier = Courier.Create(CourierName, Transport.Car, start).Value;
        
        // act
        var result = courier.Move(target);
        
        // assert
        result.IsSuccess.Should().BeTrue();
        courier.Location.Should().Be(target);
    }

    [Fact]
    public void WhenMoveToTarget_ThenShouldUpdateCurrentLocation()
    {
        // arrange
        var courier = Courier.Create(CourierName, Transport.Pedestrian, Location.Min).Value;
        var target = Location.Create(2, 1).Value;
        
        // act
        var result = courier.Move(target);
        
        // assert
        result.IsSuccess.Should().BeTrue();
        courier.Location.Should().Be(target);
    }

    [Theory]
    [ClassData(typeof(TimeToPointTestData))]
    public void WhenMoveToTarget_OnConcreteTransport_ThenShouldReachTargetForExpectedStepCount(Location start,
        Location target, Transport transport, double expectedStepCount)
    {
        // arrange
        var courier = Courier.Create(CourierName, transport, start).Value;
        
        // act
        var roundedStepCount = Math.Ceiling(expectedStepCount);
        for (var i = 0; i < roundedStepCount; i++)
        {
            var result = courier.Move(target);
            result.IsSuccess.Should().BeTrue();
        }
        
        // assert
        courier.Location.Should().Be(target);
    }
    
    [Theory]
    [ClassData(typeof(AvailableTransport))]
    public void WhenMoveToTarget_ThenShouldReachTargetForCalculatedTime(Transport transport)
    {
        // arrange
        var courier = Courier.Create(CourierName, transport, Location.Min).Value;
        var target = Location.Max;
        
        // act
        var amountOfSteps = Convert.ToInt32(courier.CalculateTimeToPoint(target).Value);
        for (var i = 0; i < amountOfSteps; i++)
        {
            var result = courier.Move(target);
            result.IsSuccess.Should().BeTrue();
        }
        
        // assert
        courier.Location.Should().Be(target);
    }
    
    private class TimeToPointTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return TestCase((2, 1), Transport.Pedestrian, 1);
            yield return TestCase((2, 2), Transport.Pedestrian, 2);
            yield return TestCase((5, 5), Transport.Pedestrian, 8);
            
            yield return TestCase((2, 1), Transport.Bicycle, 0.5);
            yield return TestCase((2, 2), Transport.Bicycle, 1);
            yield return TestCase((5, 5), Transport.Bicycle, 4);
            
            yield return TestCase((2, 1), Transport.Car, 1d / 3);
            yield return TestCase((2, 2), Transport.Car, 2d / 3);
            yield return TestCase((5, 5), Transport.Car, 8d / 3);
        }

        public object[] TestCase((int x, int y) target, Transport transport, double expectedTime) => 
            [Location.Min, Location.Create(target.x, target.y).Value, transport, expectedTime];

        IEnumerator IEnumerable.GetEnumerator() => 
            GetEnumerator();
    }
    
    private class AvailableTransport : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator() => 
            Transport
                .List()
                .Select(transport => (object[])[transport])
                .GetEnumerator();

        public object[] TestCase((int x, int y) target, Transport transport, double expectedTime) => 
            [Location.Min, Location.Create(target.x, target.y).Value, transport, expectedTime];

        IEnumerator IEnumerable.GetEnumerator() => 
            GetEnumerator();
    }
}