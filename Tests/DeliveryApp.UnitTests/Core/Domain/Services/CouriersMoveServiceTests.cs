using System;
using DeliveryApp.Core.Application.UseCases.Queries.GetCreatedAndAssignedOrders;
using DeliveryApp.Core.Domain.Model.CourierAggregate;
using DeliveryApp.Core.Domain.Model.OrderAggregate;
using DeliveryApp.Core.Domain.Services;
using FluentAssertions;
using Primitives;
using Xunit;
using Location = DeliveryApp.Core.Domain.Model.SharedKernel.Location;
using Order = DeliveryApp.Core.Domain.Model.OrderAggregate.Order;

namespace DeliveryApp.UnitTests.Core.Domain.Services;

public class CouriersMoveServiceTests
{
    [Fact]
    public void WhenMoveNULLCourier_ThenShouldReturnError_WithMessageAboutRequiredParameter()
    {
        // assert
        var couriersMove = new CouriersMoveService();
        
        // act
        var result = couriersMove.MoveOnOrder(null, null);
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(GeneralErrors.ValueIsRequired("courier"));
    }

    [Fact]
    public void WhenMoveOnNULLOrder_ThenShouldReturnError_WithMessageAboutRequiredParameter()
    {
        // assert
        var couriersMove = new CouriersMoveService();
        var courier = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;
        
        // act
        var result = couriersMove.MoveOnOrder(courier, null);
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(GeneralErrors.ValueIsRequired("order"));
    }

    [Fact]
    public void WhenMoveCourierOnOrder_AndOrderHasNotCourierId_ThenShouldReturnError()
    {
        // assert
        var couriersMove = new CouriersMoveService();
        var courier = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;
        var order = Order.Create(Guid.NewGuid(), Location.Min).Value;
        
        // act
        var result = couriersMove.MoveOnOrder(courier, order);
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(CouriersMoveService.Errors.OrderMustContainMovingCourierId());
    }

    [Fact]
    public void WhenMoveCourierOnOrder_WithSameLocation_ThenShouldCompletedOrder_AndSetFreeCourier()
    {
        // assert
        var couriersMove = new CouriersMoveService();
        var courier = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;
        var order = Order.Create(Guid.NewGuid(), Location.Min).Value;

        order.Assign(courier);
        courier.SetBusy();
        
        // act
        var result = couriersMove.MoveOnOrder(courier, order);
        
        // assert
        result.IsSuccess.Should().BeTrue();
        courier.Status.Should().Be(CourierStatus.Free);
        order.Status.Should().Be(OrderStatus.Completed);
    }
}