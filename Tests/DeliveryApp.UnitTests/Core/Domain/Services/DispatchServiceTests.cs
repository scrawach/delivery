using System;
using System.Collections;
using System.Collections.Generic;
using DeliveryApp.Core.Domain.Model.CourierAggregate;
using DeliveryApp.Core.Domain.Model.OrderAggregate;
using DeliveryApp.Core.Domain.Model.SharedKernel;
using DeliveryApp.Core.Domain.Services;
using FluentAssertions;
using Primitives;
using Xunit;

namespace DeliveryApp.UnitTests.Core.Domain.Services;

public class DispatchServiceTests
{
    [Fact]
    public void WhenDispatchNULLOrder_ThenShouldReturnError_WithMessageAboutRequiredOrderParameter()
    {
        // assert
        var dispatch = new DispatchService();
        
        // act
        var result = dispatch.Dispatch(null, null);
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(GeneralErrors.ValueIsRequired("order"));
    }

    [Fact]
    public void WhenDispatchNULLCouriersList_ThenShouldReturnError_WithMessageAboutRequiredCouriersParameter()
    {
        // assert
        var dispatch = new DispatchService();
        var order = Order.Create(Guid.NewGuid(), Location.Min);
        
        // act
        var result = dispatch.Dispatch(order.Value, null);
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(GeneralErrors.ValueIsRequired("couriers"));
    }

    [Fact]
    public void WhenDispatchOrder_ThatNotCreated_ThenShouldReturnError_WithMessageAboutInvalidOrderStatus()
    {
        // assert
        var dispatch = new DispatchService();
        var order = Order.Create(Guid.NewGuid(), Location.Min);
        var testCourier = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;
        var couriers = new[] { testCourier };

        order.Value.Assign(testCourier);
        
        // act
        var result = dispatch.Dispatch(order.Value, couriers);
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(DispatchService.Errors.OrderMustHasCreatedStatus());
    }

    [Fact]
    public void WhenDispatchOrder_WithEmptyCourierList_ThenShouldReturnError_WithMessageAboutNotFoundCourier()
    {
        // assert
        var dispatch = new DispatchService();
        var order = Order.Create(Guid.NewGuid(), Location.Min);
        var couriers = new Courier[] { };
        
        // act
        var result = dispatch.Dispatch(order.Value, couriers);
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(DispatchService.Errors.NotFoundNearestCourier());
    }
    
    [Fact]
    public void WhenDispatchOrder_OnNotFreeCouriers_ThenShouldReturnError_WithMessageAboutCourierNotFree()
    {
        // assert
        var dispatch = new DispatchService();
        var order = Order.Create(Guid.NewGuid(), Location.Min);
        var testCourier = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;
        var couriers = new[] { testCourier };
        testCourier.SetBusy();
        
        // act
        var result = dispatch.Dispatch(order.Value, couriers);
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(DispatchService.Errors.CourierMustBeFree());
    }

    [Fact]
    public void WhenDispatchOrder_OnSingleCourier_ThenShouldReturnThisSingleCourier()
    {
        // assert
        var dispatch = new DispatchService();
        var order = Order.Create(Guid.NewGuid(), Location.Min);
        var testCourier = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;
        var couriers = new[] { testCourier };
        
        // act
        var result = dispatch.Dispatch(order.Value, couriers);
        
        // assert
        result.IsSuccess.Should().BeTrue();
        result.Value.Should().Be(testCourier);
    }

    [Theory]
    [ClassData(typeof(DispatchOrderTestData))]
    public void WhenDispatchOrder_OnListOfCouriers_ThenShouldReturnNearestCourier(Order order, List<Courier> couriers, Courier expected)
    {
        // assert
        var dispatch = new DispatchService();
        
        // act
        var result = dispatch.Dispatch(order, couriers);
        
        // assert
        result.IsSuccess.Should().BeTrue();
        result.Value.Should().Be(expected);
    }
    
    private class DispatchOrderTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return TestCase1();
            yield return TestCase2();
            yield return TestCase3();
        }

        private object[] TestCase1()
        {
            var order = Order.Create(Guid.NewGuid(), Location.Min).Value;
            
            var expectedCourier = Courier.Create("bot 2", Transport.Pedestrian, Location.Min).Value;
            
            var couriers = new List<Courier>()
            {
                Courier.Create("bot 1", Transport.Pedestrian, Location.Max).Value,
                expectedCourier,
                Courier.Create("bot 3", Transport.Pedestrian, Location.Max).Value,
            };

            return
            [
                order,
                couriers,
                expectedCourier
            ];
        }
        
        private object[] TestCase2()
        {
            var order = Order.Create(Guid.NewGuid(), Location.Create(2, 2).Value).Value;
            
            var expectedCourier = Courier.Create("bot 2", Transport.Pedestrian, Location.Create(1, 2).Value).Value;
            
            var couriers = new List<Courier>()
            {
                Courier.Create("bot 1", Transport.Pedestrian, Location.Min).Value,
                Courier.Create("bot 3", Transport.Pedestrian, Location.Max).Value,
                expectedCourier,
            };

            return
            [
                order,
                couriers,
                expectedCourier
            ];
        }
        
        private object[] TestCase3()
        {
            var order = Order.Create(Guid.NewGuid(), Location.Create(7, 6).Value).Value;
            
            var expectedCourier = Courier.Create("bot 2", Transport.Pedestrian, Location.Create(5, 5).Value).Value;
            
            var couriers = new List<Courier>()
            {
                expectedCourier,
                Courier.Create("bot 1", Transport.Pedestrian, Location.Min).Value,
                Courier.Create("bot 3", Transport.Pedestrian, Location.Max).Value,
            };

            return
            [
                order,
                couriers,
                expectedCourier
            ];
        }

        IEnumerator IEnumerable.GetEnumerator() => 
            GetEnumerator();
        
    }
}