using System.Threading.Tasks;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using DeliveryApp.Infrastructure.Adapters.Postgres.Repositories;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Primitives;
using Xunit;

namespace DeliveryApp.UnitTests.Infrastructure.Postgres.Repositories;

public class OrderRepositoryTests
{
    [Fact]
    public async Task WhenAddNull_ThenShouldReturnFailureResult_WithErrorMessage_AboutRequiredParameter()
    {
        // arrange
        var orderRepository = new OrderRepository(new ApplicationDbContext(new DbContextOptions<ApplicationDbContext>()));
        
        // act
        var result = await orderRepository.AddAsync(null);
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(GeneralErrors.ValueIsRequired("order"));
    }

    [Fact]
    public void WhenUpdateNull_ThenShouldReturnFailureResult_WithErrorMessage_AboutRequiredParameter()
    {
        // arrange
        var orderRepository = new OrderRepository(new ApplicationDbContext(new DbContextOptions<ApplicationDbContext>()));
        
        // act
        var result = orderRepository.Update(null);
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(GeneralErrors.ValueIsRequired("order"));
    }

    [Fact]
    public void WhenAllWithoutSpecification_ThenShouldReturnFailureResult_WithErrorMessage_AboutRequiredParameter()
    {
        // arrange
        var orderRepository = new OrderRepository(new ApplicationDbContext(new DbContextOptions<ApplicationDbContext>()));
        
        // act
        var result = orderRepository.All(null);
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(GeneralErrors.ValueIsRequired("match"));
    }
}