using System.Threading.Tasks;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using DeliveryApp.Infrastructure.Adapters.Postgres.Repositories;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Primitives;
using Xunit;

namespace DeliveryApp.UnitTests.Infrastructure.Postgres.Repositories;

public class CourierRepositoryTests
{
    [Fact]
    public async Task WhenAddNull_ThenShouldReturnFailureResult_WithErrorMessage_AboutRequiredParameter()
    {
        // arrange
        var courierRepository = new CourierRepository(new ApplicationDbContext(new DbContextOptions<ApplicationDbContext>()));
        
        // act
        var result = await courierRepository.AddAsync(null);
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(GeneralErrors.ValueIsRequired("courier"));
    }

    [Fact]
    public void WhenUpdateNull_ThenShouldReturnFailureResult_WithErrorMessage_AboutRequiredParameter()
    {
        // arrange
        var courierRepository = new CourierRepository(new ApplicationDbContext(new DbContextOptions<ApplicationDbContext>()));
        
        // act
        var result = courierRepository.Update(null);
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(GeneralErrors.ValueIsRequired("courier"));
    }

    [Fact]
    public void WhenAllWithoutSpecification_ThenShouldReturnFailureResult_WithErrorMessage_AboutRequiredParameter()
    {
        // arrange
        var courierRepository = new CourierRepository(new ApplicationDbContext(new DbContextOptions<ApplicationDbContext>()));
        
        // act
        var result = courierRepository.All(null);
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(GeneralErrors.ValueIsRequired("match"));
    }
}