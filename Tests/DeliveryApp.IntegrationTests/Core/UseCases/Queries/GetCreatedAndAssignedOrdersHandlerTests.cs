using DeliveryApp.Core.Application.UseCases.Queries.GetCreatedAndAssignedOrders;
using DeliveryApp.Core.Domain.Model.CourierAggregate;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using DeliveryApp.Infrastructure.Adapters.Postgres.Repositories;
using DeliveryApp.IntegrationTests.Repositories;
using FluentAssertions;
using MediatR;
using Xunit;
using Location = DeliveryApp.Core.Domain.Model.SharedKernel.Location;
using Order = DeliveryApp.Core.Domain.Model.OrderAggregate.Order;

namespace DeliveryApp.IntegrationTests.Core.UseCases.Queries;

public class GetCreatedAndAssignedOrdersHandlerTests : BaseIntegrationTests
{
    protected override string DatabaseName => "orders";

    [Fact]
    public async Task WhenExecute_AndDatabaseNotContainsAnyOrders_ThenShouldReturnError_WithMessageAboutNotFound()
    {
        // arrange
        var connectionString = Container.GetConnectionString();
        var handler = new GetCreatedAndAssignedOrdersHandler(connectionString);
        var query = new GetCreatedAndAssignedOrdersQuery();
        
        // act
        var result = await handler.Handle(query, new CancellationToken());
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(GetCreatedAndAssignedOrdersHandler.Errors.NotFoundCreatedAndAssignedOrders());
    }

    [Fact]
    public async Task WhenExecute_AndDatabaseContainsCreatedAndAssignedOrders_ThenShouldReturnAllOfTheir()
    {
        // arrange
        var orderRepository = new OrderRepository(Context);
        var unitOfWork = new UnitOfWork(Context);
        var order1 = Order.Create(Guid.NewGuid(), Location.Min).Value;
        var order2 = Order.Create(Guid.NewGuid(), Location.Max).Value;

        order2.Assign(Courier.Create("bot", Transport.Car, Location.Min).Value);
        order2.Complete();
        
        await orderRepository.AddAsync(order1);
        await orderRepository.AddAsync(order2);
        await unitOfWork.SaveEntitiesAsync();
        
        var connectionString = Container.GetConnectionString();
        var handler = new GetCreatedAndAssignedOrdersHandler(connectionString);
        var query = new GetCreatedAndAssignedOrdersQuery();
        
        // act
        var result = await handler.Handle(query, new CancellationToken());
        
        // assert
        result.IsSuccess.Should().BeTrue();
        result.Value.Orders.Count.Should().Be(1);

        var firstOrder = result.Value.Orders.First();
        firstOrder.Id.Should().Be(order1.Id);
        firstOrder.Location.X.Should().Be(1);
        firstOrder.Location.Y.Should().Be(1);

    }
}