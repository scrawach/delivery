using DeliveryApp.Core.Application.UseCases.Queries.GetBusyCouriers;
using DeliveryApp.Core.Domain.Model.CourierAggregate;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using DeliveryApp.Infrastructure.Adapters.Postgres.Repositories;
using DeliveryApp.IntegrationTests.Repositories;
using FluentAssertions;
using MediatR;
using Xunit;
using Courier = DeliveryApp.Core.Domain.Model.CourierAggregate.Courier;
using Location = DeliveryApp.Core.Domain.Model.SharedKernel.Location;

namespace DeliveryApp.IntegrationTests.Core.UseCases.Queries;

public class GetBusyCouriersHandlerTests : BaseIntegrationTests
{
    protected override string DatabaseName => "couriers";

    [Fact]
    public async Task WhenGetBusyCouriers_AndDatabaseNotContainsAnyBusyCouriers_ThenShouldError_WithMessageAboutNotFound()
    {
        // assert
        var getBusyCourierHandler = new GetBusyCouriersHandler(Container.GetConnectionString());
        var query = new GetBusyCouriersQuery();
        
        // act
        var result = await getBusyCourierHandler.Handle(query, new CancellationToken());
        
        // assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(GetBusyCouriersHandler.Errors.NotFoundBusyCouriers());
    }
    
    [Fact]
    public async Task WhenGetBusyCouriers_ThenShouldReturnAllBusyCouriersFromDatabase()
    {
        // assert
        var courierRepository = new CourierRepository(Context);
        var unitOfWork = new UnitOfWork(Context);
        var courier1 = Courier.Create("important courier", Transport.Pedestrian, Location.Min).Value;
        var courier2 = Courier.Create("another courier", Transport.Car, Location.Max).Value;
        courier1.SetBusy();
        
        var expectedResponse = ExpectedResponseFrom(courier1);
        await courierRepository.AddAsync(courier1);
        await courierRepository.AddAsync(courier2);
        await unitOfWork.SaveEntitiesAsync();
        
        var getBusyCourierHandler = new GetBusyCouriersHandler(Container.GetConnectionString());
        var query = new GetBusyCouriersQuery();
        
        // act
        var result = await getBusyCourierHandler.Handle(query, new CancellationToken());
        
        // assert
        var expectedCourier = expectedResponse.Couriers.First();
        
        result.IsSuccess.Should().BeTrue();
        result.Value.Couriers.Should().NotBeNullOrEmpty();
        result.Value.Couriers.Count.Should().Be(1);

        var responseCourier = result.Value.Couriers.First();
        responseCourier.Id.Should().Be(expectedCourier.Id);
        responseCourier.Name.Should().Be(expectedCourier.Name);
        responseCourier.TransportId.Should().Be(expectedCourier.TransportId);
        responseCourier.Location.X.Should().Be(expectedCourier.Location.X);
        responseCourier.Location.Y.Should().Be(expectedCourier.Location.Y);
    }

    private GetBusyCouriersResponse ExpectedResponseFrom(Courier courier)
    {
        var location = new DeliveryApp.Core.Application.UseCases.Queries.GetBusyCouriers.Location(courier.Location.X, courier.Location.Y);
        return new GetBusyCouriersResponse([new(courier.Id, courier.Name, location, courier.Transport.Id)]);
    }
}