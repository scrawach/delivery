using DeliveryApp.Core.Domain.Model.CourierAggregate;
using DeliveryApp.Core.Domain.Model.SharedKernel;
using DeliveryApp.Core.Ports.Specifications;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using DeliveryApp.Infrastructure.Adapters.Postgres.Repositories;
using DeliveryApp.IntegrationTests.Core.UseCases.Queries;
using FluentAssertions;
using MediatR;
using Xunit;

namespace DeliveryApp.IntegrationTests.Repositories;

public class CourierRepositoryTests : BaseIntegrationTests
{
    protected override string DatabaseName => "couriers";

    [Fact]
    public async Task WhenAddCourier_ThenShouldStoredInDatabase()
    {
        // arrange
        var courier = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;
        
        // act
        var courierRepository = new CourierRepository(Context);
        await courierRepository.AddAsync(courier);

        var unitOfWork = new UnitOfWork(Context);
        await unitOfWork.SaveEntitiesAsync();
        
        // assert
        var courierFromDb = await courierRepository.GetByIdAsync(courier.Id);
        courierFromDb.IsSuccess.Should().BeTrue();
        courierFromDb.Value.Should().BeEquivalentTo(courier);
    }
    
    
    [Fact]
    public async Task WhenUpdateCourier_ThenShouldUpdatedInDatabase()
    {
        // arrange
        var courier = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;

        var courierRepository = new CourierRepository(Context);
        await courierRepository.AddAsync(courier);
        var unitOfWork = new UnitOfWork(Context);
        await unitOfWork.SaveEntitiesAsync();
        // act

        courier.SetBusy();
        courierRepository.Update(courier);
        await unitOfWork.SaveEntitiesAsync();
        
        // assert
        var courierFromDb = await courierRepository.GetByIdAsync(courier.Id);
        courierFromDb.IsSuccess.Should().BeTrue();
        courierFromDb.Value.Should().BeEquivalentTo(courier);
        courierFromDb.Value.Status.Should().Be(CourierStatus.Busy);
    }

    [Fact]
    public async Task WhenGetById_ThenShouldReturnOrderWithId()
    {
        // arrange
        var courier = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;
        
        var courierRepository = new CourierRepository(Context);
        await courierRepository.AddAsync(courier);
        
        var unitOfWork = new UnitOfWork(Context);
        await unitOfWork.SaveEntitiesAsync();

        // act
        var courierFromDb = await courierRepository.GetByIdAsync(courier.Id);
        
        // assert
        courierFromDb.IsSuccess.Should().BeTrue();
        courierFromDb.Value.Should().BeEquivalentTo(courier);
    }

    [Fact]
    public async Task WhenGetAllReadyCouriers_ThenShouldReturnOnlyReadyCouriers()
    {
        // arrange
        var readyCourier1 = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;
        var readyCourier2 = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;
        
        var busyCourier1 = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;
        var busyCourier2 = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;

        busyCourier1.SetBusy();
        busyCourier2.SetBusy();
        
        var courierRepository = new CourierRepository(Context);
        
        await courierRepository.AddAsync(readyCourier1);
        await courierRepository.AddAsync(readyCourier2);
        await courierRepository.AddAsync(busyCourier1);
        await courierRepository.AddAsync(busyCourier2);
        
        var unitOfWork = new UnitOfWork(Context);
        await unitOfWork.SaveEntitiesAsync();

        // act
        var couriersFromDb = courierRepository.All(Couriers.AreFree());
        
        // assert
        couriersFromDb.IsSuccess.Should().BeTrue();
        couriersFromDb.Value.Should().BeEquivalentTo(new [] { readyCourier1, readyCourier2 });
    }
    
    [Fact]
    public async Task WhenGetAllBusyCouriers_ThenShouldReturnOnlyBusyCouriers()
    {
        // arrange
        var readyCourier1 = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;
        var readyCourier2 = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;
        
        var busyCourier1 = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;
        var busyCourier2 = Courier.Create("bot", Transport.Pedestrian, Location.Min).Value;

        busyCourier1.SetBusy();
        busyCourier2.SetBusy();
        
        var courierRepository = new CourierRepository(Context);
        
        await courierRepository.AddAsync(readyCourier1);
        await courierRepository.AddAsync(readyCourier2);
        await courierRepository.AddAsync(busyCourier1);
        await courierRepository.AddAsync(busyCourier2);
        
        var unitOfWork = new UnitOfWork(Context);
        await unitOfWork.SaveEntitiesAsync();

        // act
        var couriersFromDb = courierRepository.All(Couriers.AreBusy());
        
        // assert
        couriersFromDb.IsSuccess.Should().BeTrue();
        couriersFromDb.Value.Should().BeEquivalentTo(new [] { busyCourier1, busyCourier2 });
    }
}