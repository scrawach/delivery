using DeliveryApp.Core.Domain.Model.CourierAggregate;
using DeliveryApp.Core.Domain.Model.OrderAggregate;
using DeliveryApp.Core.Domain.Model.SharedKernel;
using DeliveryApp.Core.Ports.Specifications;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using DeliveryApp.Infrastructure.Adapters.Postgres.Repositories;
using DeliveryApp.IntegrationTests.Core.UseCases.Queries;
using FluentAssertions;
using MediatR;
using Xunit;

namespace DeliveryApp.IntegrationTests.Repositories;

public class OrderRepositoryTests : BaseIntegrationTests
{
    protected override string DatabaseName => "orders";
    
    [Fact]
    public async Task WhenAddOrder_ThenShouldStoredInDatabase()
    {
        // arrange
        var guid = Guid.NewGuid();
        var location = Location.Min;
        var order = Order.Create(guid, location).Value;
        
        // act
        var orderRepository = new OrderRepository(Context);
        await orderRepository.AddAsync(order);

        var unitOfWork = new UnitOfWork(Context);
        await unitOfWork.SaveEntitiesAsync();
        
        // assert
        var orderFromDb = await orderRepository.GetByIdAsync(guid);
        orderFromDb.IsSuccess.Should().BeTrue();
        orderFromDb.Value.Should().BeEquivalentTo(order);
    }

    [Fact]
    public async Task WhenUpdateOrder_ThenShouldUpdatedInDatabase()
    {
        // arrange
        var guid = Guid.NewGuid();
        var location = Location.Min;
        var order = Order.Create(guid, location).Value;

        var orderRepository = new OrderRepository(Context);
        await orderRepository.AddAsync(order);
        var unitOfWork = new UnitOfWork(Context);
        await unitOfWork.SaveEntitiesAsync();

        var courier = Courier.Create("bot", Transport.Car, Location.Min).Value;
        // act

        order.Assign(courier);
        orderRepository.Update(order);
        await unitOfWork.SaveEntitiesAsync();
        
        // assert
        var orderFromDb = await orderRepository.GetByIdAsync(guid);
        orderFromDb.IsSuccess.Should().BeTrue();
        orderFromDb.Value.Should().BeEquivalentTo(order);
        orderFromDb.Value.Status.Should().Be(OrderStatus.Assigned);
    }

    [Fact]
    public async Task WhenGetById_ThenShouldReturnOrderWithId()
    {
        // arrange
        var guid = Guid.NewGuid();
        var location = Location.Min;
        var order = Order.Create(guid, location).Value;
        
        var orderRepository = new OrderRepository(Context);
        await orderRepository.AddAsync(order);
        var unitOfWork = new UnitOfWork(Context);
        await unitOfWork.SaveEntitiesAsync();
        
        // act
        var orderFromDb = await orderRepository.GetByIdAsync(guid);
        
        // assert
        orderFromDb.IsSuccess.Should().BeTrue();
        orderFromDb.Value.Should().BeEquivalentTo(order);
    }
    
    [Fact]
    public async Task WhenGetAll_CreatedOrders_ThenShouldReturnOnlyCreatedOrders()
    {
        // arrange
        var newOrder1 = Order.Create(Guid.NewGuid(), Location.Min).Value;
        var newOrder2 = Order.Create(Guid.NewGuid(), Location.Min).Value;
        
        var courier = Courier.Create("bot", Transport.Car, Location.Min).Value;
        var completedOrder1 = Order.Create(Guid.NewGuid(), Location.Min).Value;
        var completedOrder2 = Order.Create(Guid.NewGuid(), Location.Min).Value;
        completedOrder1.Assign(courier);
        completedOrder2.Assign(courier);
        
        var orderRepository = new OrderRepository(Context);
        
        await orderRepository.AddAsync(newOrder1);
        await orderRepository.AddAsync(newOrder2);
        await orderRepository.AddAsync(completedOrder1);
        await orderRepository.AddAsync(completedOrder2);
        
        var unitOfWork = new UnitOfWork(Context);
        await unitOfWork.SaveEntitiesAsync();
        
        // act
        var orderFromDb = orderRepository.All(Orders.AreCreated());
        
        // assert
        orderFromDb.IsSuccess.Should().BeTrue();
        orderFromDb.Value.Should().BeEquivalentTo(new[] { newOrder1, newOrder2 });
    }
    
    [Fact]
    public async Task WhenGetAll_AssignedOrders_ThenShouldReturnOnlyAssignedOrders()
    {
        // arrange
        var newOrder1 = Order.Create(Guid.NewGuid(), Location.Min).Value;
        var newOrder2 = Order.Create(Guid.NewGuid(), Location.Min).Value;
        
        var courier = Courier.Create("bot", Transport.Car, Location.Min).Value;
        var completedOrder1 = Order.Create(Guid.NewGuid(), Location.Min).Value;
        var completedOrder2 = Order.Create(Guid.NewGuid(), Location.Min).Value;
        completedOrder1.Assign(courier);
        completedOrder2.Assign(courier);
        
        var orderRepository = new OrderRepository(Context);
        
        await orderRepository.AddAsync(newOrder1);
        await orderRepository.AddAsync(newOrder2);
        await orderRepository.AddAsync(completedOrder1);
        await orderRepository.AddAsync(completedOrder2);
        
        var unitOfWork = new UnitOfWork(Context);
        await unitOfWork.SaveEntitiesAsync();
        
        // act
        var orderFromDb = orderRepository.All(Orders.AreAssigned());
        
        // assert
        orderFromDb.IsSuccess.Should().BeTrue();
        orderFromDb.Value.Should().BeEquivalentTo(new[] { completedOrder1, completedOrder2 });
    }

    [Fact]
    public async Task WhenSaveOrders_WithSameLocation_ThenShouldCorrectSavingAtDatabase()
    {
        // arrange
        var firstOrderId = Guid.NewGuid();
        var secondOrderId = Guid.NewGuid();
        var target = Location.Create(5, 5).Value;

        var firstOrder = Order.Create(firstOrderId, target).Value;
        var secondOrder = Order.Create(secondOrderId, target).Value;

        var orderRepository = new OrderRepository(Context);
        var unitOfWork = new UnitOfWork(Context);
        
        // act
        await orderRepository.AddAsync(firstOrder);
        await orderRepository.AddAsync(secondOrder);
        await unitOfWork.SaveEntitiesAsync();

        var savedFirstOrder = await orderRepository.GetByIdAsync(firstOrderId);
        var savedSecondOrder = await orderRepository.GetByIdAsync(secondOrderId);
        
        // arrange
        savedFirstOrder.Value.Location.Should().Be(target);
        savedSecondOrder.Value.Location.Should().Be(target);
    }
}