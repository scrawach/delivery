using DeliveryApp.Infrastructure.Adapters.Postgres;
using Microsoft.EntityFrameworkCore;
using Testcontainers.PostgreSql;
using Xunit;

namespace DeliveryApp.IntegrationTests.Repositories;

public abstract class BaseIntegrationTests : IAsyncLifetime
{
    protected ApplicationDbContext Context { get; private set; }
    
    protected PostgreSqlContainer Container { get; private set; }

    protected virtual string DatabaseName => "tableName";
    
    public async Task InitializeAsync()
    {
        Container = BuildPostgreContainer();
        await Container.StartAsync();
        
        var contextOptions = new DbContextOptionsBuilder<ApplicationDbContext>()
            .UseNpgsql(
                Container.GetConnectionString(), 
                sqlOptions => { sqlOptions.MigrationsAssembly("DeliveryApp.Infrastructure"); }).Options;

        Context = new ApplicationDbContext(contextOptions);
        await Context.Database.MigrateAsync();
    }

    public async Task DisposeAsync() => 
        await Container.DisposeAsync();

    protected virtual PostgreSqlContainer BuildPostgreContainer() =>
        new PostgreSqlBuilder()
            .WithImage("postgres:14.7")
            .WithDatabase(DatabaseName)
            .WithUsername("username")
            .WithPassword("secret")
            .WithCleanUp(true)
            .Build();
}