using CSharpFunctionalExtensions;
using Primitives;

namespace DeliveryApp.Core.Domain.Model.CourierAggregate;

public class CourierStatus : Entity<int>
{
    public static readonly CourierStatus Free = new(1, nameof(Free).ToLowerInvariant());
    public static readonly CourierStatus Busy = new(2, nameof(Busy).ToLowerInvariant());
    
    private CourierStatus(int id, string name) : base(id) => 
        Name = name;

    public string Name { get; }

    public static IEnumerable<CourierStatus> List()
    {
        yield return Free;
        yield return Busy;
    }

    public static Result<CourierStatus, Error> FromName(string name) => 
        List()
            .SingleOrDefault(WithSame(name))
            .ToResult(Errors.StatusNameNotFound(name));

    public static Result<CourierStatus, Error> FromId(int id) =>
        List()
            .SingleOrDefault(WithSame(id))
            .ToResult(Errors.StatusIdNotFound(id));
    
    private static Func<CourierStatus, bool> WithSame(string name) => 
        s => string.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase);

    private static Func<CourierStatus, bool> WithSame(int id) => 
        s => s.Id == id;

    public static class Errors
    {
        private static readonly string Tag = nameof(CourierStatus).ToLowerInvariant(); 
        
        public static Error StatusNameNotFound(string name) =>
            new($"{Tag}.name.not.found", $"Статус с именем {name} не найден. {AvailableValuesLine()}");
        
        public static Error StatusIdNotFound(int id) =>
            new($"{Tag}.id.not.found", $"Статус с Id {id} не найден. {AvailableValuesLine()}");
        
        private static string AvailableValuesLine() => 
            $"Допустимые значения: {Tag}: {string.Join(",", List().Select(s => s.Name))}";
    }
}