using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.Model.SharedKernel;
using Primitives;
using Primitives.Extensions;
using Path = DeliveryApp.Core.Domain.Model.SharedKernel.Path;

namespace DeliveryApp.Core.Domain.Model.CourierAggregate;

public class Courier : Aggregate
{
    private Courier() { }
    
    private Courier(Guid guid, string name, Transport transport, Location location) 
        : base(guid)
    {
        Name = name;
        Transport = transport;
        Location = location;
        Status = CourierStatus.Free;
    }
    
    public string Name { get; }
    
    public Transport Transport { get; }
    
    public Location Location { get; private set; }
    
    public CourierStatus Status { get; private set; }

    public UnitResult<Error> SetFree() =>
        UnitResult
            .SuccessIf(Status != CourierStatus.Free, Errors.CourierAlreadyFree())
            .Tap(() => Status = CourierStatus.Free);

    public UnitResult<Error> SetBusy() =>
        UnitResult
            .SuccessIf(Status != CourierStatus.Busy, Errors.CourierAlreadyBusy())
            .Tap(() => Status = CourierStatus.Busy);

    public UnitResult<Error> Move(Location target) => 
        NextLocationTo(target)
            .Tap(next => Location = next);

    private Result<Location, Error> NextLocationTo(Location target) =>
        Path.Create(from: Location, to: target)
            .Bind(path => path.GetPoints())
            .Map(locations => locations.Take(Transport.Speed).Last());

    public Result<double, Error> CalculateTimeToPoint(Location point) =>
        UnitResult
            .SuccessIf(point != null, GeneralErrors.ValueIsRequired(nameof(point)))
            .Map(TimeToPoint(point));

    private Func<double> TimeToPoint(Location point) =>
        () =>
        {
            var path = (double) Location.DistanceTo(point);
            return path / Transport.Speed;
        };

    public static Result<Courier, Error> Create(string name, Transport transport, Location location) => 
        Create(Guid.NewGuid(), name, transport, location);

    public static Result<Courier, Error> Create(Guid guid, string name, Transport transport, Location location) =>
        UnitResult
            .SuccessIf(guid != Guid.Empty, GeneralErrors.ValueIsRequired(nameof(guid)))
            .SuccessIf(!string.IsNullOrEmpty(name), GeneralErrors.ValueIsRequired(nameof(name)))
            .SuccessIf(transport != null, GeneralErrors.ValueIsRequired(nameof(transport)))
            .SuccessIf(location != null, GeneralErrors.ValueIsRequired(nameof(location)))
            .Map(() => new Courier(guid, name, transport, location));

    public static class Errors
    {
        private static readonly string Tag = nameof(Courier).ToLowerInvariant();

        public static Error CourierAlreadyFree() =>
            new Error($"{Tag}.already.free", "Курьер уже свободен");

        public static Error CourierAlreadyBusy() =>
            new Error($"{Tag}.already.busy", "Курьер уже занят");
    }
}