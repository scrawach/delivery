using CSharpFunctionalExtensions;
using Primitives;

namespace DeliveryApp.Core.Domain.Model.CourierAggregate;

public class Transport : Entity<int>
{
    public static readonly Transport Pedestrian = new(1, nameof(Pedestrian).ToLowerInvariant(), 1);
    public static readonly Transport Bicycle = new(2, nameof(Bicycle).ToLowerInvariant(), 2);
    public static readonly Transport Car = new(3, nameof(Car).ToLowerInvariant(), 3);
    
    private Transport(int id, string name, int speed) : base(id)
    {
        Name = name;
        Speed = speed;
    }

    public string Name { get; }
    
    public int Speed { get; }

    public static IEnumerable<Transport> List()
    {
        yield return Pedestrian;
        yield return Bicycle;
        yield return Car;
    }

    public static Result<Transport, Error> FromName(string name) => 
        List()
            .SingleOrDefault(WithSame(name))
            .ToResult(Errors.StatusNameNotFound(name));

    public static Result<Transport, Error> FromId(int id) =>
        List()
            .SingleOrDefault(WithSame(id))
            .ToResult(Errors.StatusIdNotFound(id));
    
    private static Func<Transport, bool> WithSame(string name) => 
        s => string.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase);

    private static Func<Transport, bool> WithSame(int id) => 
        s => s.Id == id;

    public static class Errors
    {
        private static readonly string Tag = nameof(Transport).ToLowerInvariant(); 
        
        public static Error StatusNameNotFound(string name) =>
            new($"{Tag}.name.not.found", $"Статус с именем {name} не найден. {AvailableValuesLine()}");
        
        public static Error StatusIdNotFound(int id) =>
            new($"{Tag}.id.not.found", $"Статус с Id {id} не найден. {AvailableValuesLine()}");
        
        private static string AvailableValuesLine() => 
            $"Допустимые значения: {Tag}: {string.Join(",", List().Select(s => s.Name))}";
    }

}