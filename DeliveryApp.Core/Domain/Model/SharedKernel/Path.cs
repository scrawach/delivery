using CSharpFunctionalExtensions;
using Primitives;
using Primitives.Extensions;

namespace DeliveryApp.Core.Domain.Model.SharedKernel;

public class Path : ValueObject
{
    private Path(Location start, Location target)
    {
        Start = start;
        Target = target;
    }

    public Location Start { get; }
    
    public Location Target { get; }
    
    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return Start;
        yield return Target;
    }

    public Result<IEnumerable<Location>, Error> GetPoints()
    {
        var points = new List<Location>();

        foreach (var pos in BuildPath(Start, Target))
        {
            var location = Location.Create(pos.x, pos.y);

            if (location.IsFailure)
                return location.Error;
            
            points.Add(location.Value);
        }

        if (points.Count == 0)
            points.Add(Target);
        
        return points;
    }

    private static IEnumerable<(int x, int y)> BuildPath(Location from, Location to)
    {
        const int maxStep = 1;
        
        var currentX = from.X;
        var currentY = from.Y;

        while (to.X != currentX || to.Y != currentY)
        {
            var xDirection = to.X - currentX;
            var yDirection = to.Y - currentY;

            var xStep = Math.Clamp(xDirection, -maxStep, maxStep);
            var yStep = Math.Clamp(yDirection, -maxStep, maxStep);
            
            if (xStep != 0)
            {
                currentX += xStep;
                yield return (currentX, currentY);
            }

            if (yStep != 0)
            {
                currentY += yStep;
                yield return (currentX, currentY);
            }
        }
    }

    public static Result<Path, Error> Create(Location from, Location to) =>
        UnitResult
            .SuccessIf(from != null, GeneralErrors.ValueIsRequired(nameof(from)))
            .SuccessIf(to != null, GeneralErrors.ValueIsRequired(nameof(to)))
            .Map(() => new Path(from, to));
}