using CSharpFunctionalExtensions;
using Primitives;
using Primitives.Extensions;

namespace DeliveryApp.Core.Domain.Model.SharedKernel;

public class Location : ValueObject
{
    public static Location Min => new(1, 1);
    public static Location Max => new(10, 10);
    
    private Location(int x, int y)
    {
        X = x;
        Y = y;
    }

    public int X { get; }
    
    public int Y { get; }
    
    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return X;
        yield return Y;
    }

    public int DistanceTo(Location target) => 
        Math.Abs(target.X - X) + Math.Abs(target.Y - Y);

    public static Result<Location, Error> Create(int x, int y) =>
        UnitResult
            .SuccessIf(IsValueInRange(x, Min.X, Max.X), GeneralErrors.ValueIsInvalid(nameof(x)))
            .SuccessIf(IsValueInRange(y, Min.Y, Max.Y), GeneralErrors.ValueIsInvalid(nameof(y)))
            .Map(() => new Location(x, y));

    public static Result<Location> Random(Random randomizer)
    {
        var x = randomizer.Next(Min.X, Max.X + 1);
        var y = randomizer.Next(Min.Y, Max.Y + 1);
        return new Location(x, y);
    }

    private static bool IsValueInRange(int value, int min, int max) =>
        value >= min && value <= max;
}