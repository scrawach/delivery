using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.Model.CourierAggregate;
using DeliveryApp.Core.Domain.Model.OrderAggregate.DomainEvents;
using DeliveryApp.Core.Domain.Model.SharedKernel;
using Primitives;
using Primitives.Extensions;

namespace DeliveryApp.Core.Domain.Model.OrderAggregate;

public class Order : Aggregate
{
    private Order() { }
    
    private Order(Guid guid, Location location) : base(guid)
    {
        Location = location;
        Status = OrderStatus.Created;
    }

    public Location Location { get; }
    
    public OrderStatus Status { get; private set; }
    
    public Guid? CourierId { get; private set; }

    public UnitResult<Error> Complete() =>
        UnitResult
            .SuccessIf(Status == OrderStatus.Assigned, Errors.OrderNotAssigned())
            .Tap(() => ChangeStatusTo(OrderStatus.Completed));

    public UnitResult<Error> Assign(Courier courier) =>
        UnitResult
            .SuccessIf(courier != null, GeneralErrors.ValueIsRequired(nameof(courier)))
            .SuccessIf(Status != OrderStatus.Completed, Errors.OrderAlreadyCompleted())
            .Tap(() => CourierId = courier.Id)
            .Tap(() => ChangeStatusTo(OrderStatus.Assigned));

    private void ChangeStatusTo(OrderStatus target)
    {
        var previous = Status;
        Status = target;
        RaiseDomainEvent(new OrderStatusChangedDomainEvent(previous, target));
    }
    
    public static Result<Order, Error> Create(Guid id, Location location) =>
        UnitResult
            .SuccessIf(id != Guid.Empty, GeneralErrors.ValueIsRequired(nameof(id)))
            .SuccessIf(location != null, GeneralErrors.ValueIsRequired(nameof(location)))
            .Map(() => new Order(id, location));

    public static class Errors
    {
        private static readonly string Tag = $"{nameof(Order).ToLowerInvariant()}";
        
        public static Error OrderAlreadyCompleted() => 
            new($"{Tag}.already.completed", "Заказ уже завершён");
        
        public static Error OrderNotAssigned() => 
            new($"{Tag}.not.assigned", "Заказ не был назначен");
    }
}