﻿using Primitives;

namespace DeliveryApp.Core.Domain.Model.OrderAggregate.DomainEvents;

public record OrderStatusChangedDomainEvent(OrderStatus PreviousStatus, OrderStatus CurrentStatus) : DomainEvent
{
}