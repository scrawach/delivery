using CSharpFunctionalExtensions;
using Primitives;

namespace DeliveryApp.Core.Domain.Model.OrderAggregate;

public class OrderStatus : Entity<int>
{
    public static readonly OrderStatus Created = new(1, nameof(Created).ToLowerInvariant());
    public static readonly OrderStatus Assigned = new(2, nameof(Assigned).ToLowerInvariant());
    public static readonly OrderStatus Completed = new(3, nameof(Completed).ToLowerInvariant());

    public OrderStatus() { }
    
    private OrderStatus(int id, string name) : base(id) => 
        Name = name;

    public string Name { get; }

    public static IEnumerable<OrderStatus> List()
    {
        yield return Created;
        yield return Assigned;
        yield return Completed;
    }

    public static Result<OrderStatus, Error> FromName(string name) => 
        List()
            .SingleOrDefault(WithSame(name))
            .ToResult(Errors.StatusNameNotFound(name));

    public static Result<OrderStatus, Error> FromId(int id) =>
        List()
            .SingleOrDefault(WithSame(id))
            .ToResult(Errors.StatusIdNotFound(id));
    
    private static Func<OrderStatus, bool> WithSame(string name) => 
        s => string.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase);

    private static Func<OrderStatus, bool> WithSame(int id) => 
        s => s.Id == id;

    public static class Errors
    {
        private static readonly string Tag = nameof(OrderStatus).ToLowerInvariant(); 
        
        public static Error StatusNameNotFound(string name) =>
            new($"{Tag}.name.not.found", $"Статус с именем {name} не найден. {AvailableValuesLine()}");
        
        public static Error StatusIdNotFound(int id) =>
            new($"{Tag}.id.not.found", $"Статус с Id {id} не найден. {AvailableValuesLine()}");
        
        private static string AvailableValuesLine() => 
            $"Допустимые значения: {Tag}: {string.Join(",", List().Select(s => s.Name))}";
    }
}