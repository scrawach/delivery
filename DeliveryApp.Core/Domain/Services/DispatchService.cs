using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.Model.CourierAggregate;
using DeliveryApp.Core.Domain.Model.OrderAggregate;
using DeliveryApp.Core.Domain.Model.SharedKernel;
using Primitives;

namespace DeliveryApp.Core.Domain.Services;

public class DispatchService : IDispatchService
{
    public Result<Courier, Error> Dispatch(Order order, IReadOnlyList<Courier> couriers) =>
        order
            .ToResult(GeneralErrors.ValueIsRequired(nameof(order)))
            .Ensure(o => o.Status == OrderStatus.Created, Errors.OrderMustHasCreatedStatus())
            .Ensure(_ => couriers != null, GeneralErrors.ValueIsRequired(nameof(couriers)))
            .Bind(_ => SelectNearestCourier(order.Location, couriers));


    private Result<Courier, Error> SelectNearestCourier(Location target, IReadOnlyList<Courier> couriers)
    {
        var minTime = double.MaxValue;
        Courier selected = null;
        
        foreach (var courier in couriers)
        {
            if (courier.Status != CourierStatus.Free)
                return Errors.CourierMustBeFree();
            
            var time = courier.CalculateTimeToPoint(target);

            if (time.IsFailure)
                return time.Error;
            
            if (time.Value < minTime)
            {
                minTime = time.Value;
                selected = courier;
            }
        }

        return selected.ToResult(Errors.NotFoundNearestCourier());
    }
    
    public class Errors
    {
        private static readonly string Tag = nameof(DispatchService).ToLowerInvariant();

        public static Error OrderMustHasCreatedStatus() =>
            new Error($"{Tag}.order.not.created", "Заказ не создан или уже был назначен");
        
        public static Error CourierMustBeFree() =>
            new Error($"{Tag}.courier.not.free", "Курьер не свободен");
        
        public static Error NotFoundNearestCourier() =>
            new Error($"{Tag}.courier.not.found", "Не найден ближайший курьер");

    }
}