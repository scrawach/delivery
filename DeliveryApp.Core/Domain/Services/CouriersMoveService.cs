using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.Model.CourierAggregate;
using DeliveryApp.Core.Domain.Model.OrderAggregate;
using Primitives;

namespace DeliveryApp.Core.Domain.Services;

public class CouriersMoveService : ICouriersMoveService
{
    public UnitResult<Error> MoveOnOrder(Courier courier, Order order)
    {
        if (courier == null) return GeneralErrors.ValueIsRequired(nameof(courier));
        if (order == null) return GeneralErrors.ValueIsRequired(nameof(order));
        if (order.CourierId != courier.Id) return Errors.OrderMustContainMovingCourierId();
        
        var move = courier.Move(order.Location);

        if (move.IsFailure)
            return move;

        if (order.Location == courier.Location)
        {
            order.Complete();
            courier.SetFree();
        }
        
        return UnitResult.Success<Error>();
    }

    public static class Errors
    {
        public static Error OrderMustContainMovingCourierId() =>
            new($"{nameof(CouriersMoveService)}.order.must.contains.correct.courier_id",
                "Заказ должен содержать Id доставщика");
    }
}