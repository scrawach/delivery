using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.Model.CourierAggregate;
using DeliveryApp.Core.Domain.Model.OrderAggregate;
using Primitives;

namespace DeliveryApp.Core.Domain.Services;

public interface ICouriersMoveService
{
    UnitResult<Error> MoveOnOrder(Courier courier, Order order);
}