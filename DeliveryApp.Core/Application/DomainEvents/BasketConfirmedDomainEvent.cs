using Primitives;

namespace DeliveryApp.Core.Application.DomainEvents;

public record BasketConfirmedDomainEvent : DomainEvent
{
    public BasketConfirmedDomainEvent(Guid guid, string street)
    {
        EventId = guid;
        Street = street;
    }

    public string Street { get; set; }
}