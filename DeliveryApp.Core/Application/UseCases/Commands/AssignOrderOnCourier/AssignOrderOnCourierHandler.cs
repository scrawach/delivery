using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.Services;
using DeliveryApp.Core.Ports;
using DeliveryApp.Core.Ports.Specifications;
using MediatR;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Commands.AssignOrderOnCourier;

public class AssignOrderOnCourierHandler : IRequestHandler<AssignOrderOnCourierCommand, UnitResult<Error>>
{
    private readonly IDispatchService _dispatchService;
    private readonly IOrderRepository _orderRepository;
    private readonly ICourierRepository _courierRepository;
    private readonly IUnitOfWork _unitOfWork;

    public AssignOrderOnCourierHandler(
        IDispatchService dispatchService,
        IOrderRepository orderRepository,
        ICourierRepository courierRepository, 
        IUnitOfWork unitOfWork)
    {
        _dispatchService = dispatchService;
        _orderRepository = orderRepository;
        _courierRepository = courierRepository;
        _unitOfWork = unitOfWork;
    }

    public async Task<UnitResult<Error>> Handle(AssignOrderOnCourierCommand request, CancellationToken cancellationToken)
    {
        var couriers = _courierRepository.All(Couriers.AreFree());

        if (couriers.IsFailure)
            return couriers;
        
        var orders = _orderRepository.All(Orders.AreCreated());

        if (orders.IsFailure)
            return orders;

        if (!orders.Value.Any() || !couriers.Value.Any())
            return Errors.NotFoundActors();
        
        var firstOrder = orders.Value.First();
        var nearestCourier = _dispatchService.Dispatch(firstOrder, couriers.Value.ToList());
        
        if (nearestCourier.IsFailure)
            return nearestCourier;
        
        var assignResult = firstOrder.Assign(nearestCourier.Value);
        
        if (assignResult.IsFailure)
            return assignResult;

        var courierSetBusy = nearestCourier.Value.SetBusy();

        if (courierSetBusy.IsFailure)
            return courierSetBusy;
        
        _orderRepository.Update(firstOrder);
        _courierRepository.Update(nearestCourier.Value);

        await _unitOfWork.SaveEntitiesAsync(cancellationToken);
        
        return UnitResult.Success<Error>();
    }

    public static class Errors
    {
        public static Error NotFoundActors() => 
            new($"{nameof(AssignOrderOnCourierCommand)}.not.found.actors", "Акторы не найдены");
    }
}