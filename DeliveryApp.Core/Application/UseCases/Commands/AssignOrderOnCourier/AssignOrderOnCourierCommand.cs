using CSharpFunctionalExtensions;
using MediatR;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Commands.AssignOrderOnCourier;

public class AssignOrderOnCourierCommand : IRequest<UnitResult<Error>>
{
    
}