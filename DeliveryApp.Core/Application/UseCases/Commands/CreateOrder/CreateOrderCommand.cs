using CSharpFunctionalExtensions;
using MediatR;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Commands.CreateOrder;

public class CreateOrderCommand : IRequest<UnitResult<Error>>
{
    public CreateOrderCommand(Guid basketId, string street)
    {
        if (basketId == Guid.Empty) throw new ArgumentOutOfRangeException(nameof(basketId));
        if (string.IsNullOrEmpty(street)) throw new ArgumentOutOfRangeException(nameof(street));
        
        BasketId = basketId;
        Street = street;
    }

    public Guid BasketId { get; }
    
    public string Street { get; }
}