using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.Model.OrderAggregate;
using DeliveryApp.Core.Domain.Model.SharedKernel;
using DeliveryApp.Core.Ports;
using MediatR;
using Primitives;
using ArithmeticException = System.ArithmeticException;

namespace DeliveryApp.Core.Application.UseCases.Commands.CreateOrder;

public class CreateOrderHandler : IRequestHandler<CreateOrderCommand, UnitResult<Error>>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IGeoService _geoService;
    private readonly IUnitOfWork _unitOfWork;

    public CreateOrderHandler(IOrderRepository orderRepository, IUnitOfWork unitOfWork, IGeoService geoService)
    {
        _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
        _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        _geoService = geoService ?? throw new ArithmeticException(nameof(geoService));
    }

    public async Task<UnitResult<Error>> Handle(CreateOrderCommand request, CancellationToken cancellationToken) =>
        await GetLocationByStreet(request.Street)
            .Bind(location => Order.Create(request.BasketId, location))
            .Bind(order => _orderRepository.AddAsync(order))
            .Tap(() => _unitOfWork.SaveEntitiesAsync(cancellationToken));

    private Task<Result<Location, Error>> GetLocationByStreet(string street) =>
        street
            .ToResult(GeneralErrors.ValueIsRequired(nameof(street)))
            .Bind(streetName => _geoService.GetLocationFromStreet(streetName));
}