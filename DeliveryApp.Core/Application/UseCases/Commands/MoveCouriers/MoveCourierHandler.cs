using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.Services;
using DeliveryApp.Core.Ports;
using DeliveryApp.Core.Ports.Specifications;
using MediatR;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Commands.MoveCouriers;

public class MoveCourierHandler : IRequestHandler<MoveCourierCommand, UnitResult<Error>>
{
    private readonly IOrderRepository _orderRepository;
    private readonly ICourierRepository _courierRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly ICouriersMoveService _couriersMove;

    public MoveCourierHandler(
        IOrderRepository orderRepository, 
        ICourierRepository courierRepository,
        IUnitOfWork unitOfWork,
        ICouriersMoveService couriersMove)
    {
        _orderRepository = orderRepository;
        _courierRepository = courierRepository;
        _unitOfWork = unitOfWork;
        _couriersMove = couriersMove;
    }

    public async Task<UnitResult<Error>> Handle(MoveCourierCommand request, CancellationToken cancellationToken)
    {
        var orders = _orderRepository.All(Orders.AreAssigned());

        if (orders.IsFailure)
            return orders;

        var couriers = _courierRepository.All(Couriers.AreBusy());

        if (couriers.IsFailure)
            return couriers;

        var dictionaryOfCouriers = couriers.Value.ToDictionary(key => key.Id, value => value);
        var listOfOrders = orders.Value.ToList();

        foreach (var order in listOfOrders)
        {
            var courierId = order.CourierId.Value;
            var assignedCouriers = dictionaryOfCouriers[courierId];

            var result = _couriersMove.MoveOnOrder(assignedCouriers, order);

            if (result.IsFailure)
                return result;
            
            _courierRepository.Update(assignedCouriers);
            _orderRepository.Update(order);
        }

        await _unitOfWork.SaveEntitiesAsync(cancellationToken);
        return UnitResult.Success<Error>();
    }
}