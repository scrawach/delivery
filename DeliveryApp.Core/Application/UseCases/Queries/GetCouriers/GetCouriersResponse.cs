namespace DeliveryApp.Core.Application.UseCases.Queries.GetCouriers;

public class GetCouriersResponse
{
    public GetCouriersResponse(List<Courier> couriers)
    {
        Couriers.AddRange(couriers);
    }

    public List<Courier> Couriers { get; set; } = new();
}

public class Courier
{
    public Courier(Guid id, string name, Location location, int transportId)
    {
        Id = id;
        Name = name;
        Location = location;
        TransportId = transportId;
    }

    public Guid Id { get; set; }

    public string Name { get; set; }

    public Location Location { get; set; }
    
    public int TransportId { get; set; }
}

public class Location
{
    public Location(int x, int y)
    {
        X = x;
        Y = y;
    }

    public int X { get; set; }

    public int Y { get; set; }
}