using CSharpFunctionalExtensions;
using MediatR;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Queries.GetCouriers;

public class GetCouriersQuery : IRequest<Result<GetCouriersResponse, Error>>
{
    
}