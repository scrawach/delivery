namespace DeliveryApp.Core.Application.UseCases.Queries.GetCreatedAndAssignedOrders;

public class GetCreatedAndAssignedOrdersResponse
{
    public GetCreatedAndAssignedOrdersResponse(List<Order> orders)
    {
        Orders.AddRange(orders);
    }

    public List<Order> Orders { get; set; } = new();
}

public class Order
{
    public Order(Guid id, Location location)
    {
        Id = id;
        Location = location;
    }

    public Guid Id { get; set; }
    
    public Location Location { get; set; }
}

public class Location
{
    public Location(int x, int y)
    {
        X = x;
        Y = y;
    }

    public int X { get; set; }
    
    public int Y { get; set; }
}