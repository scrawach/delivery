using CSharpFunctionalExtensions;
using Dapper;
using DeliveryApp.Core.Domain.Model.OrderAggregate;
using MediatR;
using Npgsql;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Queries.GetCreatedAndAssignedOrders;

public class GetCreatedAndAssignedOrdersHandler : IRequestHandler<GetCreatedAndAssignedOrdersQuery,
    Result<GetCreatedAndAssignedOrdersResponse, Error>>
{
    private readonly string _connectionString;

    public GetCreatedAndAssignedOrdersHandler(string connectionString) => 
        _connectionString = !string.IsNullOrEmpty(connectionString) 
            ? connectionString 
            : throw new ArgumentNullException(nameof(connectionString));
    
    public async Task<Result<GetCreatedAndAssignedOrdersResponse, Error>> Handle(GetCreatedAndAssignedOrdersQuery request, CancellationToken cancellationToken)
    {
        await using var conection = new NpgsqlConnection(_connectionString);
        await conection.OpenAsync(cancellationToken);

        var results = await conection.QueryAsync(
            @"SELECT o.id as order_id, o.location_x, o.location_y
                 FROM public.orders as o
                 WHERE o.status_id=@created_id or o.status_id=@assigned_id", 
            new {created_id = OrderStatus.Created.Id, assigned_id = OrderStatus.Assigned.Id});

        var listOfResults = results.AsList();
        
        return listOfResults.Count == 0 
            ? Result.Failure<GetCreatedAndAssignedOrdersResponse, Error>(Errors.NotFoundCreatedAndAssignedOrders()) 
            : new GetCreatedAndAssignedOrdersResponse(MapOrders(listOfResults).ToList());
    }

    private IEnumerable<Order> MapOrders(IEnumerable<dynamic> results) => 
        from item in results 
        let location = new Location(item.location_x, item.location_y) 
        select new Order(item.order_id, location);

    public static class Errors
    {
        public static Error NotFoundCreatedAndAssignedOrders() => 
            new($"{nameof(GetCreatedAndAssignedOrdersHandler)}.not.found.created.and.assigned.couriers", "Не найдено не завершенных заказов");
    }
}