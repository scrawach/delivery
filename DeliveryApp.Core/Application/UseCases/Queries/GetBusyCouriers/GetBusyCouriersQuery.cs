using CSharpFunctionalExtensions;
using MediatR;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Queries.GetBusyCouriers;

public class GetBusyCouriersQuery : IRequest<Result<GetBusyCouriersResponse, Error>>
{
    
}