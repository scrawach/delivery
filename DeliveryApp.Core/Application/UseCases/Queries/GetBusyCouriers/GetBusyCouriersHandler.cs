using CSharpFunctionalExtensions;
using Dapper;
using DeliveryApp.Core.Domain.Model.CourierAggregate;
using MediatR;
using Npgsql;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Queries.GetBusyCouriers;

public class GetBusyCouriersHandler : IRequestHandler<GetBusyCouriersQuery, Result<GetBusyCouriersResponse, Error>>
{
    private readonly string _connectionString;

    public GetBusyCouriersHandler(string connectionString) => 
        _connectionString = !string.IsNullOrEmpty(connectionString) 
            ? connectionString 
            : throw new ArgumentNullException(nameof(connectionString));

    public async Task<Result<GetBusyCouriersResponse, Error>> Handle(GetBusyCouriersQuery request, CancellationToken cancellationToken)
    {
        using var conection = new NpgsqlConnection(_connectionString);
        await conection.OpenAsync(cancellationToken);

        var results = await conection.QueryAsync(
            @"SELECT c.id as courier_id, c.name, c.transport_id, c.location_x, c.location_y
                 FROM public.couriers as c
                 WHERE c.status_id=@status_id", new {status_id = CourierStatus.Busy.Id});

        var listOfResults = results.AsList();
        
        return listOfResults.Count == 0 
            ? Result.Failure<GetBusyCouriersResponse, Error>(Errors.NotFoundBusyCouriers()) 
            : new GetBusyCouriersResponse(MapCouriers(listOfResults).ToList());
    }

    private IEnumerable<Courier> MapCouriers(IEnumerable<dynamic> results) => 
        from item in results 
        let location = new Location(item.location_x, item.location_y) 
        select new Courier(item.courier_id, item.name, location, item.transport_id);

    public static class Errors
    {
        public static Error NotFoundBusyCouriers() => 
            new($"{nameof(GetBusyCouriersHandler)}.not.found.busy.couriers", "Не найдено занытых курьеров");
    }
}