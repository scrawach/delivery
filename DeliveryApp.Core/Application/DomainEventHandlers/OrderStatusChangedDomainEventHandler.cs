﻿using DeliveryApp.Core.Domain.Model.OrderAggregate.DomainEvents;
using DeliveryApp.Core.Ports;
using MediatR;

namespace DeliveryApp.Core.Application.DomainEventHandlers;

public class OrderStatusChangedDomainEventHandler : INotificationHandler<OrderStatusChangedDomainEvent>
{
    private readonly IBusProducer _bus;

    public OrderStatusChangedDomainEventHandler(IBusProducer bus) => 
        _bus = bus ?? throw new ArgumentNullException(nameof(bus));

    public async Task Handle(OrderStatusChangedDomainEvent domainEvent, CancellationToken token) => 
        await _bus.Publish(domainEvent, token);
}