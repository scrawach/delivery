using DeliveryApp.Core.Application.DomainEvents;
using DeliveryApp.Core.Application.UseCases.Commands.CreateOrder;
using MediatR;

namespace DeliveryApp.Core.Application.DomainEventHandlers;

public class BasketConfirmedDomainEventHandler : INotificationHandler<BasketConfirmedDomainEvent>
{
    private readonly IMediator _mediator;

    public BasketConfirmedDomainEventHandler(IMediator mediator) => 
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));

    public async Task Handle(BasketConfirmedDomainEvent notification, CancellationToken cancellationToken)
    {
        var createOrderCommand = new CreateOrderCommand(notification.EventId, notification.Street);
        await _mediator.Send(createOrderCommand, cancellationToken);
    }
}