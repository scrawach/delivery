using DeliveryApp.Core.Domain.Model.CourierAggregate;

namespace DeliveryApp.Core.Ports.Specifications;

public static class Couriers
{
    public static ISpecification<Courier> AreFree() => 
        new DirectSpecification<Courier>(courier => courier.Status == CourierStatus.Free);

    public static ISpecification<Courier> AreBusy() => 
        new DirectSpecification<Courier>(courier => courier.Status == CourierStatus.Busy);
}