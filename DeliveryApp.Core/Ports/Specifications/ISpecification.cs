using System.Linq.Expressions;

namespace DeliveryApp.Core.Ports.Specifications;

public interface ISpecification<TObject>
{
    Expression<Func<TObject, bool>> Expression { get; }
}