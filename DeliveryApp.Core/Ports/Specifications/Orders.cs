using DeliveryApp.Core.Domain.Model.OrderAggregate;

namespace DeliveryApp.Core.Ports.Specifications;

public static class Orders
{
    public static ISpecification<Order> AreCreated() =>
        new DirectSpecification<Order>(order => order.Status == OrderStatus.Created);

    public static ISpecification<Order> AreAssigned() =>
        new DirectSpecification<Order>(order => order.Status == OrderStatus.Assigned);
}