using System.Linq.Expressions;

namespace DeliveryApp.Core.Ports.Specifications;

public class DirectSpecification<TObject> : ISpecification<TObject>
{
    public DirectSpecification(Expression<Func<TObject, bool>> predicate) => 
        Expression = predicate;

    public Expression<Func<TObject, bool>> Expression { get; }
}