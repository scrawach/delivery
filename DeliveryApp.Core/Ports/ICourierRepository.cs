using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.Model.CourierAggregate;
using DeliveryApp.Core.Ports.Specifications;
using Primitives;

namespace DeliveryApp.Core.Ports;

public interface ICourierRepository : IRepository<Courier>
{
    Task<UnitResult<Error>> AddAsync(Courier courier);
    UnitResult<Error> Update(Courier courier);
    Task<Result<Courier, Error>> GetByIdAsync(Guid id);
    Result<IEnumerable<Courier>, Error> All(ISpecification<Courier> match);
}