using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.Model.OrderAggregate;
using DeliveryApp.Core.Ports.Specifications;
using Primitives;

namespace DeliveryApp.Core.Ports;

public interface IOrderRepository : IRepository<Order>
{
    Task<UnitResult<Error>> AddAsync(Order order);
    UnitResult<Error> Update(Order order);
    Task<Result<Order, Error>> GetByIdAsync(Guid id);
    Result<IEnumerable<Order>, Error> All(ISpecification<Order> match);
}