﻿using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.Model.OrderAggregate.DomainEvents;
using Primitives;

namespace DeliveryApp.Core.Ports;

public interface IBusProducer
{
    Task<UnitResult<Error>> Publish(OrderStatusChangedDomainEvent domainEvent, CancellationToken token = default);
}