﻿using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.Model.SharedKernel;
using Primitives;

namespace DeliveryApp.Core.Ports;

public interface IGeoService
{
    Task<Result<Location, Error>> GetLocationFromStreet(string street, CancellationToken token = default);
}